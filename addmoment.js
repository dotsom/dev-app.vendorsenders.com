var express             = require('express')
,   app                 = express()
,   port                = process.env.PORT || 8080
,   mongoose            = require('mongoose')
,   passport            = require('passport')
,   localStrategy       = require('passport-local').Strategy
,   flash               = require('connect-flash')
,   configDB            = require('./config/database.js')
,   bcrypt              = require('bcrypt-nodejs')
,   validator           = require('validator')
,   _                   = require('underscore')
,   Encoder             = require('node-html-encoder').Encoder
,   encoder             = new Encoder('entity')
,   mandrill            = require('node-mandrill')('7BzcCNailMlLHtGQ8xkheQ')
,   crypto               = require('crypto')

// Schema
,   Availability        = require('./app/schema/bkSchema.Availability')
,   Booking             = require('./app/schema/bkSchema.Booking')
,   Host                = require('./app/schema/bkSchema.Host')
,   Notification        = require('./app/schema/bkSchema.Notification')
,   Waitlist            = require('./app/schema/bkSchema.Waitlist')
,   User                = require('./app/schema/bkSchema.User')
,   PwToken             = require('./app/schema/bkSchema.PwToken');

//Connect to DB
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('Successful connection to mongodb at ' + configDB.url);
  Availability.find({}, function(err, avails){
    _.each(avails, function(avail){

        avail.moment = new Date(Number(avail.year), Number(avail.month), Number(avail.day));

        avail.save(function(err){
            if (err) {
                console.log(err);
            } else {
                console.log(avail);
            }
        });
    });
  });
});