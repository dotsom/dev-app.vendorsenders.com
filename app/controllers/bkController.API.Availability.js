module.exports = function(
    app,
    passport,
    mongoose,
    _,
    Availability,
    Host
) {

    //GET request for all availabilities
    app.get('/api/availability', function(req, res) {
        Availability.find(function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });

    //GET request for a single availability
    app.get('/api/availability/:_id', function(req, res) {
        Availability.findById(req.params._id, function(err, availability){
            if (err) {
                res.send(err);
            } else {
                res.json(availability);
            }
        });
    });

    //GET request for a single year
    app.get('/api/availability/year/:year', function(req, res) {
        Availability.find({
            year : parseInt(req.params.year, 10)
        }, function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });

    //GET request for a single month
    app.get('/api/availability/year/:year/month/:month', function(req, res) {
        Availability.find({
            month : parseInt(req.params.month, 10),
            year : parseInt(req.params.year, 10)
        }, function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                res.json(availabilities);
            }
        });
    });

    //POST request for to check max visits
    app.post('/api/availability/max', function(req, res) {

        var query = {
            moment: {
              $gte: req.body.yearBegin,
              $lt: req.body.yearEnd
            },
            uid : req.body.uid,
            hid : req.body.hid,
            booked : true,
        };

        Availability.find(query, function(err, availabilities){
            if (err) {
                res.send(err);
            } else {
                Host.findById(req.body.hid, function(err, host){

                    console.log(availabilities);

                    if (err) {
                        res.send(err);
                    } else {

                        var bookingCount = 0;
                        _.each(availabilities, function(booking){
                            bookingCount++;
                        });

                        var visitInfo = {lastYear: bookingCount, maxVisits: host.restrictions.max, avails: availabilities};
                        console.log(visitInfo);
                        res.send(200, visitInfo);
                    }
                });
            }
        });
    });
};