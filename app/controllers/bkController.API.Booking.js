module.exports = function(
    app,
    passport,
    mongoose,
    Booking
) {

    //GET request for all bookings
    app.get('/api/booking', function(req, res) {
        Booking.find(function(err, bookings){
            if (err) {
                res.send(err);
            } else {
                res.json(bookings);
            }
        });
    });

    //GET request for bookings for an _id
    app.get('/api/booking/:_id', function(req, res) {
        Booking.findById(req.params._id, function(err, booking){
            if (err) {
                res.send(err);
            } else {
                res.json(booking);
            }
        });
    });

    //GET request for bookings for a uid
    app.get('/api/booking/uid/:uid', function(req, res) {
        Booking.find({uid: req.params.uid}, function(err, bookings){
            if (err) {
                res.send(err);
            } else {
                res.json(bookings);
            }
        });
    });

    //GET request for bookings for an aid
    app.get('/api/booking/aid/:aid', function(req, res) {
        Booking.findOne({aid: req.params.aid}, function(err, booking){
            if (err) {
                res.send(err);
            } else {
                res.json(booking);
            }
        });
    });

    //GET request for bookings for an hid
    app.get('/api/booking/hid/:hid', function(req, res) {
        Booking.find({hid: req.params.hid}, function(err, bookings){
            if (err) {
                res.send(err);
            } else {
                res.json(bookings);
            }
        });
    });
};