module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    appRoot,
// Schema
    Host,
    User,
    File,
// Models
    RouteMdl
) {

var path = require('path'),
    fs = require('fs'),
    ID = function () {
        return Math.random().toString(36).substr(2, 9);
    };

    app.get('/api/files/:uid', function(req, res) {
        console.log('hi');
        process.nextTick(function() {
            File.find({uid: mongoose.Types.ObjectId(req.params.uid)},function(err, files){
                if (err) {
                    res.send(err);
                } else {
                    res.json(files);
                }
            });
        });
    });

    app.post('/api/files/:hid', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            process.nextTick(function() {
            var tempPath = req.files.upload.path,
                tempName = ID();
                targetPath = appRoot + '/public/uploads/host/files/' + tempName + path.extname(req.files.upload.name);
                fs.rename(tempPath, targetPath, function(err) {
                    if (err) {
                        throw err;
                    } else {
                        var newFile = new File();
                        newFile.path = '/uploads/host/files/' + tempName + path.extname(req.files.upload.name);
                        newFile.name = req.body.name ? req.body.name : req.files.upload.name;
                        newFile.type = req.files.upload.type;
                        newFile.uid = req.body.hid;

                        newFile.save(function(err){
                            if (err) {
                                res.send(200, {err: err});
                                console.log(err);
                            } else {
                                res.redirect('/admin/host-mgmt/' + req.body.hid);
                                console.log("Successful File Save!");
                            }
                        });
                    }
                });
            });
        }
    });

    app.delete('/api/files/:id', function(req, res) {
        process.nextTick(function() {
            File.findById(req.params.id,function(err, file){
                if (err) {
                    res.send(err);
                } else {
                    file.remove(function(err){
                        res.json({message: 'success'});
                    });
                }
            });
        });
    });
};