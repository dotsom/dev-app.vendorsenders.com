module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    extend,
// Schema
    Availability,
    Booking,
    File,
    Host,
    User,
    Waitlist,
// Models
    RouteMdl
) {



    //GET request for all hosts
    app.get('/api/hosts', function(req, res) {
        Host.find(function(err, hosts){
            if (err) {
                res.send(err);
            } else {
                res.json(hosts);
            }
        });
    });

    //GET request for a single host
    app.get('/api/hosts/:_id', function(req, res) {
        Host.findById(req.params._id, function(err, host){
            if (err) {
                res.send(err);
            } else {
                res.json(host);
            }
        });
    });

    app.post('/api/hosts', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            process.nextTick(function() {
                Host.findOne({ 'profile.email': req.body.email }, function(err, host) {
                    // error
                    if (err) {
                        res.location('/new-host');
                        res.send(200, {signupErrorId: 1, err: err});
                    }

                    // already taken
                    if (host) {
                        res.location('/new-host');
                        res.send(200, {signupErrorId: 2, err: 'That email address already has a company associated with it.'});
                    }

                    // create host
                    else {
                        var newHost            = new Host(),
                            errorCount         = 0,
                            errorMsgs          = [];

                        // set the user's local credentials
                        // sanitize as we go.
                        //
                        console.log(req.body);

                        if (validator.isEmail(validator.toString(req.body.email))) {
                            newHost.profile.email = String(req.body.email);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid email address.');
                        }

                        if (validator.isLength(req.body.password, 8)) {
                            newHost.profile.password = newHost.generateHash(req.body.password);
                        } else {
                            errorCount++;
                            errorMsgs.push('Passwords must be at least 8 characters.');
                        }

                        if (req.body.name != "") {
                            newHost.company.name = String(req.body.name);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a name for this company.');
                        }

                        if (req.body.phone != "") {
                            newHost.company.phone = String(req.body.phone);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid phone number.');
                        }

                        if (req.body.address != "") {
                            newHost.company.address = String(req.body.address1);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter an address for this company.');
                        }

                        if (req.body.city != "") {
                            newHost.company.city = String(req.body.city);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a city for this company.');
                        }

                        if (req.body.state != "") {
                            newHost.company.state = String(req.body.state);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a state for this company.');
                        }

                        if (req.body.zip != "") {
                            newHost.company.zip = String(req.body.zip);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a zip code for this company.');
                        }

                        if (req.body.fee != "") {
                            newHost.company.fee = Number(req.body.fee);
                        }

                        if (req.body.size != "") {
                            newHost.company.size = Number(req.body.size);
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a size for this company.');
                        }

                        if (req.body.max != "") {
                            newHost.restrictions.max = Number(req.body.max);
                        }

                        if (req.body.items != "") {
                            newHost.restrictions.items = String(req.body.items);
                        }

                        newHost.profile.accountStatus     = Number(1);

                        // save the host
                        if (errorCount == 0) {
                            console.log('New Host:\n', newHost);
                            newHost.save(function(err) {
                                if (err) {
                                    res.send(200, {signupErrorId: 5, err: errorMsgs});
                                } else {
                                    User.findOne({'local.email': newHost.profile.email}, function(err, user){
                                        if (!user) {
                                            var newUser = new User();
                                            newUser.local = {
                                                accountStatus: 2,
                                                accountType: 2,
                                                phone: newHost.company.phone,
                                                firstName: 'Host',
                                                lastName: 'Company',
                                                password: newHost.profile.password,
                                                email: newHost.profile.email
                                            };
                                            newUser.bus = {
                                                hid: newHost._id
                                            };
                                            newUser.save(function(err){
                                                if (err) {
                                                    res.send(200, {signupErrorId: 3, err: errorMsgs});
                                                } else {
                                                    res.send(200, {
                                                        signupErrorId: false,
                                                        host: newHost,
                                                        user: newUser
                                                    });
                                                }
                                            });
                                        } else {
                                            res.send(200, {signupErrorId: 6, err: 'That email is already associated with an account.'});
                                        }
                                    });
                                }
                            });
                        } else {
                            res.send(200, {signupErrorId: 4, err: errorMsgs});
                        }
                    }
                });
            });
        } else {
            res.send(200, {signupErrorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        }
    });

    app.put('/api/hosts/:_id', function(req, res) {
        //if (RouteMdl.isAdmin(req, res)){
            process.nextTick(function() {
                Host.findById(req.params._id, function(err,host){
                    if (err) {
                        res.send(err);
                    } else {
                        extend(host.profile, req.body.profile);
                        extend(host.company, req.body.company);
                        extend(host.restrictions, req.body.restrictions);

                        host.save(function(err){
                            if (err) {
                                res.send(200, {errorId: 5, err: err});
                            } else {
                                User.findOne({bus: {hid: host._id}}, function(err, user){
                                    if (err) {
                                        res.send(200, {errorId: 5, err: err});
                                    } else {
                                        var extender = {
                                                email: host.profile.email,
                                                phone: host.company.phone,
                                                password: user.generateHash(host.profile.password)
                                        };

                                        extend(user.local, extender);

                                        user.save(function(err){
                                            if (err) {
                                                res.send(200, {errorId: 5, err: err});
                                            } else {
                                                res.send(200, {
                                                    errorId: false,
                                                    user: user,
                                                    host: host,
                                                    message: "The update was successful"
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });
        //} else {
            //res.send(200, {errorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        //}
    });

    //DELETE a user
    app.delete('/api/hosts/:_id', function(req, res){
        if (RouteMdl.isAdmin(req, res)){
            User.remove({
                bus: { hid: mongoose.Types.ObjectId('5407f1dd9f98c3e037481f3d') }
            }, function(err){
                if (err) {
                    res.send(err);
                } else {
                    Booking.remove({
                        hid: req.params._id
                    }, function(err){
                        if (err) {
                            res.send(err);
                        } else {
                            Availability.remove({
                                hid: req.params._id
                            }, function(err){
                                if (err) {
                                    res.send(err);
                                } else {
                                    File.remove({
                                        uid: req.params._id
                                    }, function(err){
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            Waitlist.remove({
                                                hid: req.params._id
                                            }, function(err){
                                                Host.findById(req.params._id).remove(function(){
                                                    res.json({
                                                        message: "Deleted host as well as all associated bookings, files, users, availabilities, and wailists",
                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            res.send(200, {signupErrorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        }
    });
};