module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    extend,
    _,
// Schema
    Notification,
// Models
    RouteMdl
) {

    //GET request for notifications for a specific user
    app.get('/api/notifications/uid/:uid', function(req, res) {
        Notification.find({uid: req.params.uid}).sort('date').exec(function(err, notifications){
            if (err) {
                res.send(err);
            } else {
                res.json(notifications);
            }
        });
    });

    //GET request for a single notification
    app.get('/api/notifications/:_id', function(req, res) {
        Notification.findById(req.params._id, function(err, notification){
            if (err) {
                res.send(err);
            } else {
                res.json(notification);
            }
        });
    });

    app.post('/api/notifications', function(req, res) {
        process.nextTick(function() {
            var newNotification = new Notification(),
                errorCount = 0,
                errorMsgs = [];

                newNotification.uid = req.body.uid;
                newNotification.message = req.body.message;
                newNotification.messageType = req.body.messageType;
                newNotification.read = {};

                _.each(newNotification.uid, function(uid){
                    newNotification.read[uid] = false;
                });

            // save the notification
            if (errorCount == 0) {
                newNotification.save(function(err) {
                    if (err) {
                        res.send(200, {errorId: 1, err: err});
                    } else {
                        res.send(200, {errorId: false, notification: newNotification});
                    }
                });
            } else {
                res.send(200, {errorId: 4, err: errorMsgs});
            }
        });
    });


    /*app.put('/api/notifications/read/:uid', function(req, res) {
        process.nextTick(function() {
            var beenRead = {
                read : {}
            };
            beenRead.read[req.params.uid] = true;
            Notification.update({uid: req.params.uid}, beenRead, function(err){
                if (err) {
                    res.send(200, {err: err});
                } else {
                    res.send(200, beenRead);
                }
            });
        });
    });*/
};