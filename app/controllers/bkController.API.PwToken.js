module.exports = function(
    app,
    mongoose,
    validator,
    mandrill,
    crypto,
// Schema
    User,
    PwToken,
// Models
    RouteMdl
) {

    //POST to generate a new token and send email with URL
    app.post('/api/pwtoken', function(req, res) {
        console.log(req.body);
        User.findOne({ 'local.email' :  req.body.email },function(err, user){
            if (err) {
                res.send(err);
            } else if (!user) {
                res.send({message: 'No user with that email address exists.'});
            } else {
                PwToken.findOne({uid: user._id}, function(err, pwtoken){
                    if (pwtoken){
                        pwtoken.remove();
                    }

                    var new_pwtoken = new PwToken();
                    crypto.randomBytes(20, function(err, buf){
                        new_pwtoken.uid = user._id;
                        new_pwtoken.token = buf.toString('hex');
                        new_pwtoken.expires = Date.now() + 86400000;

                        var message_html = "<h2>Reset Your Password</h2>"
                        + "<p>We see that you need to reset your password.  You can use the link provided below to reset within the next 24 hours.  After that this URL will expire and you will have to start the reset process over again.  Thank you for using Vendor Senders!</p>"
                        + "<p>Use this link: " + req.headers.host + '/reset/' + new_pwtoken.token + "</p>";

                        var message = {
                            "html": message_html,
                            "subject": "Reset your password",
                            "from_email": "message.noreply@vendorsenders.com",
                            "from_name": "Vendor Senders",
                            "to": [
                                {
                                    "email": user.local.email,
                                    "name": user.local.firstName + " " + user.local.lastName,
                                    "type": "to"
                                }
                            ],
                            "headers": {
                                "Reply-To": "message.noreply@vendorsenders.com"
                            }
                        };
                        var email = {
                            "key": "7BzcCNailMlLHtGQ8xkheQ",
                            "message": message,
                            "async": false,
                            "ip_pool": "Main Pool"
                        };

                        new_pwtoken.save(function(err){
                            if (err) {
                                res.send(err);
                            } else {
                                res.send(200, {message: 'token generated'});
                                mandrill('/messages/send', email, function(err, m_res){
                                    if (err) {
                                        res.send(err);
                                    } else {
                                        res.send(200, {message: 'email generated'});
                                    }
                                });
                            }
                        });
                    });
                });
            }
        });
    });

    app.delete('/api/pwtoken/:token', function(req, res) {
        PwToken.remove({ 'token' :  req.params.token },function(err, user){
            if (err) {
                res.send(err);
            } else {
                res.send({message: 'Token removed'});
            }
        });
    });
};