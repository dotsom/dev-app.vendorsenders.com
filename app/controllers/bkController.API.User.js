module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    _,
    encoder,
    mandrill,
// Schema
    Availability,
    Booking,
    File,
    Host,
    User,
    Waitlist,
// Models
    CalendarMdl,
    RouteMdl
) {

    //GET request for all users
    app.get('/api/users', function(req, res) {
        User.find(function(err, users){
            if (err) {
                res.send(err);
            } else {
                var allUsers = [];
                _.each(users, function(user){
                    var thisUser = {
                        _id: user._id,
                        __v: user.__v,
                        local: {
                            accountStatus: user.local.accountStatus,
                            accountType: user.local.accountType,
                            phone: user.local.phone,
                            lastName: user.local.lastName,
                            firstName: user.local.firstName,
                            email: user.local.email
                        }
                    }
                    if (typeof user.bus != 'undefined') {
                        thisUser.bus = user.bus;
                    }
                    allUsers.push(thisUser);
                });
                res.json(allUsers);
            }
        });
    });

    //GET request for all real users.  Excludes host account proxies.
    app.get('/api/users/true', function(req, res) {
        User.find({"local.accountType" : {"$ne": 2}}).exec(function(err, users){
            if (err) {
                res.send(err);
            } else {
                var allUsers = [];
                _.each(users, function(user){
                    var thisUser = {
                        _id: user._id,
                        __v: user.__v,
                        local: {
                            accountStatus: user.local.accountStatus,
                            accountType: user.local.accountType,
                            phone: user.local.phone,
                            lastName: user.local.lastName,
                            firstName: user.local.firstName,
                            email: user.local.email
                        }
                    }
                    if (typeof user.bus != 'undefined') {
                        thisUser.bus = user.bus;
                    }
                    allUsers.push(thisUser);
                });
                res.json(allUsers);
            }
        });
    });

    //GET request for all uids of a type
    app.get('/api/users/type/:type', function(req, res) {
        User.find({"local.accountType": parseInt(req.params.type)}, function(err, users){
            if (err) {
                res.send(err);
            } else {
                var allUsers = [];
                _.each(users, function(user){
                    var thisUid = user._id;
                    allUsers.push(thisUid);
                });
                res.json(allUsers);
            }
        });
    });

    //GET request for a single user
    app.get('/api/users/:_id', function(req, res) {
        User.findById(req.params._id, function(err, user){
            if (err) {
                res.send(err);
            } else {
                var thisUser = {
                    _id: user._id,
                    __v: user.__v,
                    local: {
                        accountStatus: user.local.accountStatus,
                        accountType: user.local.accountType,
                        phone: user.local.phone,
                        lastName: user.local.lastName,
                        firstName: user.local.firstName,
                        email: user.local.email
                    }
                }
                if (typeof user.bus != 'undefined') {
                    thisUser.bus = user.bus;
                }
                res.json(thisUser);
            }
        });
    });

    //GET uid for an hid
    app.get('/api/users/hid/:hid', function(req, res) {
        User.findOne({bus: {hid: mongoose.Types.ObjectId(req.params.hid)}}, function(err, user){
            if (err) {
                res.send(err);
            } else {
                var thisUser =  user._id;
                res.json(thisUser);
            }
        });
    });

    // POST a new user
    app.post('/api/users', function(req, res) {
        process.nextTick(function() {
            User.findOne({ 'local.email' :  req.body.local.email }, function(err, user) {
                // error
                if (err) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 1, err: err});
                }

                // already taken
                if (user) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 2, err: 'That email address is already in use.'});
                }

                // create user
                else {
                    // if there is no user with that email
                    // create the user
                    var newUser            = new User(),
                        errorCount         = 0,
                        errorMsgs          = [];

                        newUser.bus        = {};
                        newUser.local      = {};

                    // set the user's local credentials
                    // sanitize as we go.
                    if (validator.isEmail(validator.toString(req.body.local.email))) {
                        newUser.local.email = encoder.htmlEncode(String(req.body.local.email));
                        console.log("Valid email!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid email address.');
                    }

                    if (validator.isLength(req.body.local.password, 8)) {
                        newUser.local.password = newUser.generateHash(req.body.local.password);
                        console.log("Valid password!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Passwords must be at least 8 characters.');
                    }

                    if (req.body.local.firstName != "") {
                        newUser.local.firstName = encoder.htmlEncode(String(req.body.local.firstName));
                        console.log("Valid first name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your first name.');
                    }

                    if (req.body.local.lastName != "") {
                        newUser.local.lastName = encoder.htmlEncode(String(req.body.local.lastName));
                        console.log("Valid last name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your last name.');
                    }

                    if (req.body.local.phone != "") {
                        newUser.local.phone = encoder.htmlEncode(String(req.body.local.phone));
                        console.log("Valid phone number!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid phone number.');
                    }

                    if (req.body.local.contactMethod != "") {
                        newUser.local.contactMethod = encoder.htmlEncode(String(req.body.local.contactMethod));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your preferred method of communication.');
                    }

                    if (req.body.local.paymentMethod != "") {
                        newUser.local.paymentMethod = Number(req.body.local.paymentMethod);
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter the manner in which you will provide your payment details.');
                    }

                    if (req.body.bus.name != "") {
                        newUser.bus.name = encoder.htmlEncode(String(req.body.bus.name));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your business name.');
                    }

                    if (req.body.bus.info != "") {
                        newUser.bus.info = encoder.htmlEncode(String(req.body.bus.info));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a business description.');
                    }

                    if (req.body.bus.type != "") {
                        newUser.bus.type = encoder.htmlEncode(String(req.body.bus.type));
                    } else {
                        errorCount++;
                        errorMsgs.push('Please select a business type.');
                    }

                    newUser.local.accountType       = Number(1);
                    newUser.local.accountStatus     = Number(1);

                    // save the user
                    if (errorCount == 0) {
                        console.log('New User:\n', newUser);
                        newUser.save(function(err) {
                            if (err) {
                                res.send(200, {signupErrorId: 3, err: err});
                                console.log(err);
                            } else {

                                console.log("Successful Save!");

                                var reference = {
                                    payment: ["Phone", "Email"]
                                }

                                var message_html = "<h2>New Application</h2>"
                                + "<p>A new application has been submitted at Vendor Senders.  </p>"
                                + "<ul><li>Name: " + newUser.local.firstName + " " + newUser.local.lastName + "</li>"
                                + "<li>Email: " + newUser.local.email + "</li>"
                                + "<li>phone: " + newUser.local.phone + "</li>"
                                + "<li>contact by: " + newUser.local.contactMethod + "</li>"
                                + "<li>collect payment info by: " + reference.payment[newUser.local.paymentMethod] + "</li>"
                                + "<li>contact by: " + newUser.local.contactMethod + "</li>";

                                var message = {
                                    "html": message_html,
                                    "subject": "A New Application",
                                    "from_email": "message.noreply@vendorsenders.com",
                                    "from_name": "Vendor Senders",
                                    "to": [
                                        {
                                            "email": "vendorsenders@gmail.com",
                                            "name": "Vendor Senders",
                                            "type": "to"
                                        }
                                    ],
                                    "headers": {
                                        "Reply-To": "message.noreply@vendorsenders.com"
                                    }
                                };
                                var email = {
                                    "key": "d9LAsHodRN6b4fwOtbhA8A",
                                    "message": message,
                                    "async": false,
                                    "ip_pool": "Main Pool"
                                };


                                mandrill('/messages/send', email, function(err, m_res){
                                    if (err) {
                                        res.send(err);
                                        console.log("MAIL ERROR: " + err);
                                    } else {
                                        res.send(200, {signupErrorId: false, user: newUser});
                                    }
                                });
                            }
                        });
                    } else {
                        res.send(200, {signupErrorId: 4, err: errorMsgs});
                    }
                }
            });
        });
    });

    //PUT request to update a user
    app.put('/api/users/:_id', function(req, res) {
        if (RouteMdl.isAdmin(req, res) || RouteMdl.isThisUser(req, req.params._id)){
            User.findById(req.params._id, function(err, user){
                if (err) {
                    res.send(err);
                } else {
                    if (req.body.phone) {
                        if (req.body.phone != "") {
                            user.local.phone = encoder.htmlEncode(String(req.body.phone));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid phone number.');
                        }
                    }
                    if (req.body.firstName) {
                        if (req.body.firstName != "") {
                            user.local.firstName = encoder.htmlEncode(String(req.body.firstName));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter your first name.');
                        }
                    }
                    if (req.body.lastName) {
                        if (req.body.lastName != "") {
                            user.local.lastName = encoder.htmlEncode(String(req.body.lastName));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter your last name.');
                        }
                    }
                    if (req.body.email) {
                        if (validator.isEmail(validator.toString(req.body.email))) {
                            user.local.email = encoder.htmlEncode(String(req.body.email));
                        } else {
                            errorCount++;
                            errorMsgs.push('Please enter a valid email address.');
                        }
                    }

                    if (req.body.password) {
                        if (validator.isLength(req.body.password, 8)) {
                            user.local.password = user.generateHash(req.body.password);
                        } else {
                            errorCount++;
                            errorMsgs.push('Passwords must be at least 8 characters.');
                        }
                    }

                    if (typeof user.bus != 'undefined') {
                        if (req.body.bname) {
                            if (req.body.bname != "") {
                                var userbusname = encoder.htmlEncode(String(req.body.bname));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please enter your business name.');
                            }
                        } else if (typeof user.bus.name != 'undefined') {
                            var userbusname = user.bus.name;
                        } else {
                            var userbusname = "";
                        }
                        if (req.body.binfo) {
                            if (req.body.binfo != "") {
                                userbusinfo = encoder.htmlEncode(String(req.body.binfo));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please enter a business description.');
                            }
                        } else if (typeof user.bus.info != 'undefined') {
                            var userbusinfo = user.bus.info;
                        } else {
                            var userbusinfo = "";
                        }
                        if (req.body.btype) {
                            if (req.body.btype != "") {
                                userbustype = encoder.htmlEncode(String(req.body.btype));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please select a business type.');
                            }
                        } else if (typeof user.bus.type != 'undefined') {
                            var userbustype = user.bus.type;
                        } else {
                            var userbustype = "";
                        }
                    } else {
                        if (req.body.bname) {
                            if (req.body.bname != "") {
                                var userbusname = encoder.htmlEncode(String(req.body.bname));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please enter your business name.');
                            }
                        } else {
                            var userbusname = "";
                        }
                        if (req.body.binfo) {
                            if (req.body.binfo != "") {
                                userbusinfo = encoder.htmlEncode(String(req.body.binfo));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please enter a business description.');
                            }
                        } else {
                            var userbusinfo = "";
                        }
                        if (req.body.btype) {
                            if (req.body.btype != "") {
                                userbustype = encoder.htmlEncode(String(req.body.btype));
                            } else {
                                errorCount++;
                                errorMsgs.push('Please select a business type.');
                            }
                        } else {
                            var userbustype = "";
                        }
                    }

                    user.bus = {
                        name: userbusname,
                        info: userbusinfo,
                        type: userbustype
                    };

                    user.save(function(err){
                        if (err) {
                            res.send(err);
                        } else {
                            if (userbustype){
                                var availQuery = {uid: user._id};
                                var availChanges = {vendorType: userbustype};
                                Availability.update(availQuery, availChanges, {multi: true}, function(err, num){
                                    res.json({
                                        availsUpdated: num,
                                        message: "The update was successful",
                                        user: user
                                    });
                                });
                            }
                        }
                    });
                }
            });
        }
    });

    //PUT request to update a user
    app.put('/api/users/unlock/:_id', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            User.findById(req.params._id, function(err, user){
                if (err) {
                    res.send(err);
                } else {
                    user.local.accountStatus = 2;
                    user.save(function(err){
                        if (err) {
                            res.send(err);
                        } else {
                            res.json({
                                message: "This account has been activated.",
                                user: user
                            });
                        }
                    });
                }
            });
        }
    });

    //PUT request to update a user
    app.put('/api/users/lock/:_id', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            User.findById(req.params._id, function(err, user){
                if (err) {
                    res.send(err);
                } else {
                    user.local.accountStatus = 1;
                    user.save(function(err){
                        if (err) {
                            res.send(err);
                        } else {
                            res.json({
                                message: "This account has been locked.",
                                user: user
                            });
                        }
                    });
                }
            });
        }
    });

    //DELETE a user
    app.delete('/api/users/:_id', function(req, res){
        if (RouteMdl.isAdmin(req, res)){
            User.remove({
                _id: req.params._id
            }, function(err){
                if (err) {
                    res.send(err);
                } else {
                    Booking.remove({
                        uid: req.params._id
                    }, function(err){
                        if (err) {
                            res.send(err);
                        } else {
                            Availability.update(
                                { booked: true, uid: req.params._id },
                                {
                                    $set: { booked: false },
                                    $unset: { uid: 1, bid: 1 }
                                },
                            function(err){
                                if (err) {
                                    res.send(err);
                                } else {
                                    File.remove({
                                        uid: req.params._id
                                    }, function(err){
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            Waitlist.update(
                                            { users :  req.params._id },
                                            { $pull: { users: req.params._id } },
                                            function(){
                                                Waitlist.find({users: {$size: 0}}).remove(function(){
                                                    res.json({
                                                        message: "Deleted user as well as all associated bookings, files.  Waitlist entries updated.",
                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            res.send(200, {signupErrorId: 5, err: 'You do not have the correct permissions to complete this request.'});
        }
    });
}