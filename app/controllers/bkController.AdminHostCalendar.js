module.exports = function(
    app,
    passport,
    mongoose,
    validator,
    _,
    encoder,
// Schema
    Availability,
    Booking,
    Host,
    User,
// Models
    CalendarMdl,
    RouteMdl
) {

    //GET request for a single user
    app.get('/admin/host-calendar/:_id', function(req, res) {
        // Ensure admin authentication
        if (RouteMdl.isAdmin(req, res)){

            var serverData = {};
            var d = new Date();

            //Find specified user, set all values to thisUser object
            Host.findById(req.params._id, function(err, host){
                if (err) {
                    res.send(err);
                } else {
                    console.log('got a host');
                    serverData.host = host;
                    Availability.find({
                        month : d.getMonth(),
                        year : d.getFullYear(),
                        hid : host._id
                    }, function(err, availabilities){
                        var cal_string = CalendarMdl.createMarkupNoPlus(d.getMonth(), d.getFullYear());
                        var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();
                        Booking.find({hid: host._id}, function(err, bookings){

                            serverData.today = d;
                            serverData.bookings = bookings;
                            serverData.avail = availabilities;
                            serverData.Calendar = CalendarMdl;
                            serverData.currentYear = d.getFullYear();
                            serverData.currentMonth = d.getMonth();
                            serverData.calendarString = cal_string;

                            var renderData = {
                                user : req.user,
                                host : host,
                                calendar : cal_string,
                                calendarHeading : cal_heading_string,
                                JSONdata : JSON.stringify(serverData),
                                serverData : serverData
                            };
                            res.render('admin-host-calendar.ejs', renderData);
                        });
                    });
                }
            });
        } else {
            res.redirect('/');
        }
    });
};