module.exports = function(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    ) {

    app.get('/admin/host-mgmt', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            res.render('admin-host-mgmt.ejs', {query: false, user: req.user});
        } else {
            res.redirect('/');
        }
    });
    app.get('/admin/host-mgmt/:hid', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            res.render('admin-host-mgmt.ejs', {query: req.params.hid, user: req.user});
        } else {
            res.redirect('/');
        }
    });
};