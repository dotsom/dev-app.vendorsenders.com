module.exports = function(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    ) {

    app.get('/admin/user-mgmt', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            res.render('admin-user-mgmt.ejs', {query: false, user: req.user});
        } else {
            res.redirect('/');
        }
    });

    app.get('/admin/user-mgmt/:uid', function(req, res) {
        if (RouteMdl.isAdmin(req, res)){
            res.render('admin-user-mgmt.ejs', {query: req.params.uid, user: req.user});
        } else {
            res.redirect('/');
        }
    });
};