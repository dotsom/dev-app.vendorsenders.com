module.exports = function(
        app,
        _,
    // Schema
        Host,
        Availability,
        Booking,
        User,
        Waitlist,
    // Models
        CalendarMdl,
        RouteMdl,
        MailMdl
    ) {

    var hours = [
        ['12', 'AM'],
        ['1', 'AM'],
        ['2', 'AM'],
        ['3', 'AM'],
        ['4', 'AM'],
        ['5', 'AM'],
        ['6', 'AM'],
        ['7', 'AM'],
        ['8', 'AM'],
        ['9', 'AM'],
        ['10', 'AM'],
        ['11', 'AM'],
        ['12', 'PM'],
        ['1', 'PM'],
        ['2', 'PM'],
        ['3', 'PM'],
        ['4', 'PM'],
        ['5', 'PM'],
        ['6', 'PM'],
        ['7', 'PM'],
        ['8', 'PM'],
        ['9', 'PM'],
        ['10', 'PM'],
        ['11', 'PM']
    ];

var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

    var ObjectId = require('mongoose').Types.ObjectId;
    var changedMonth;

    app.get('/calendar', function(req, res) {
        var serverData = {};
        var d = new Date();
        var cal_string = CalendarMdle.createMarkup(d.getMonth(), d.getFullYear());
        var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();

        res.render('calendar.ejs', {
            data: JSON.stringify({
                today: d,
                Calendar: CalendarMdl,
                currentYear: d.getFullYear(),
                currentMonth: d.getMonth()
            }),
            calendar: cal_string,
            calendarHeading: cal_heading_string
        });
    });

    /**
     * Handle POST Requests
     */
    app.post('/calendar', function(req, res) {
        if (RouteMdl.isLoggedIn(req, res)){
            process.nextTick(function() {
                switch (req.body.reqType) {

                    // CHANGE MONTH ON CALENDAR
                    case "changeMonth":
                        changedMonth = CalendarMdl.changeMonth(Number(req.body.currentMonth),Number(req.body.currentYear));
                        if (changedMonth.newCalendarMdl) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthNoPlus":
                        changedMonth = CalendarMdl.changeMonth(Number(req.body.currentMonth),Number(req.body.currentYear), false);
                        if (changedMonth.newCalendar) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthArrow":
                        changedMonth = CalendarMdl.changeMonthArrow(Number(req.body.currentMonth),Number(req.body.currentYear), req.body.direction);
                        if (changedMonth.newCalendar) {
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthArrowNoPlus":
                        changedMonth = CalendarMdl.changeMonthArrow(Number(req.body.currentMonth),Number(req.body.currentYear), req.body.direction, false);
                        if (changedMonth.newCalendar) {
                            console.log(changedMonth.newMonth, changedMonth.newYear);
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear
                            }, function(err, availabilities){
                                console.log(availabilities);
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    case "changeMonthHostView":
                        changedMonth = CalendarMdl.changeMonthArrow(Number(req.body.currentMonth),Number(req.body.currentYear), req.body.direction, false);
                        if (changedMonth.newCalendar) {
                            console.log(changedMonth.newMonth, changedMonth.newYear);
                            Availability.find({
                                month : changedMonth.newMonth,
                                year : changedMonth.newYear,
                                hid : req.body.hid
                            }, function(err, availabilities){
                                console.log(availabilities);
                                changedMonth.avails = availabilities;
                                res.send(200, changedMonth);
                            });
                        }
                    break;

                    // ADD NEW AVAILABILITY
                    case "addAvail":
                        var newAvail = new Availability();
                        newAvail.month = req.body.month;
                        newAvail.year = req.body.year;
                        newAvail.day = req.body.day;
                        newAvail.hid = req.body.hid;
                        newAvail.start = req.body.start;
                        newAvail.end = req.body.end;
                        newAvail.booked = false;
                        newAvail.moment = new Date(Number(req.body.year), Number(req.body.month), Number(req.body.day));

                        newAvail.save(function(err) {
                            if (err) {
                                res.send(200, {err: err, status: "failure"});
                                console.log(err);
                            } else {
                                res.send(200, {avail: newAvail, status: "success"});
                            }
                        });
                    break;

                    // DELETE AVAILABILITY
                    case "deleteAvail":
                        Availability.remove({
                            _id : ObjectId(req.body.aid)
                        }, function(err){
                            if (err) {
                                res.send(200, {err: err, status: "failure"});
                                console.log(err);
                            } else {
                                res.send(200, {status: "success"});
                            }
                        });
                    break;

                    // ADD NEW BOOKING
                    case "addBooking":
                        process.nextTick(function() {
                            Booking.findOne({ 'aid' :  req.body.aid }, function(err, booking) {
                                if (err) {
                                    res.send(200, {errorId: 1, err: err});
                                } else if (booking) {
                                    res.send(200, {errorId: 2, err: 'That availability has already been booked.'});
                                } else {
                                    var newBooking = new Booking();
                                    newBooking.aid = req.body.aid;
                                    newBooking.hid = req.body.hid;
                                    newBooking.uid = req.body.uid;

                                    newBooking.save(function(err) {
                                        if (err) {
                                            res.send(200, {err: err, status: "failure"});
                                            console.log(err);
                                        } else {
                                            Availability.findOne({
                                                _id : newBooking.aid
                                            }, function(err, availability){
                                                availability.booked = true;
                                                availability.uid = req.body.uid;
                                                availability.bid = newBooking._id;
                                                availability.vendorType = req.body.type;
                                                availability.save(function(err){
                                                    if (err) {
                                                        res.send(200, {err: err, status: "failure"});
                                                        console.log(err);
                                                    } else {
                                                        res.send(200, {
                                                            avail : availability,
                                                            booking: newBooking,
                                                            status: "success"
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    break;

                    // DELETE BOOKING
                    case "deleteBooking":
                        process.nextTick(function() {
                            Booking.findOne({ _id : ObjectId(req.body.bid)}, function(err, booking) {
                                if (booking) {
                                    var thisAid = booking.aid;
                                    if (err) {
                                        res.send(200, {errorId: 1, err: err});
                                    } else if (!booking) {
                                        res.send(200, {errorId: 2, err: 'A booking with that ID does not exist.'});
                                    } else {
                                        booking.remove(function(err){
                                            if (err) {
                                                res.send(200, {err: err, status: "failure"});
                                                console.log(err);
                                            } else {
                                                Availability.findById(thisAid, function(err, avail){

                                                    avail.booked = false;
                                                    avail.uid = '';
                                                    avail.bid = '';

                                                    avail.save(function(err){
                                                        if (err) {
                                                            res.send(200, {err: err, status: "failure"});
                                                            console.log(err);
                                                        } else {
                                                            res.send(200, {
                                                                aid: thisAid,
                                                                status: "success"
                                                            });

                                                            Waitlist.findOne({aid : thisAid}, function(err, waitlist) {
                                                                if (waitlist) {
                                                                    Host.findById(avail.hid, function(err, host){
                                                                        var message = "<h2>Waitlisted Booking Available!</h2>"
                                                                        + "<p>You are receiving this email because a waitlisted location on the Vendor Senders online calendar has become available for booking.  Any vendor who is waitlisted for this date and location has been notified and bookings are first come first serve.   Go to vendorsenders.com and check out the new availability in "
                                                                        + host.company.city
                                                                        + " at "
                                                                        + host.company.name
                                                                        + ".  The space is available from "
                                                                        + hours[avail.start][0] + " " + hours[avail.start][1] + " until "
                                                                        + hours[avail.end][0] + " " + hours[avail.end][1]
                                                                        + " on " + months[avail.month] + ' ' + avail.day + ', ' + avail.year + ".</p>";

                                                                        var subject = "Waitlisted Appointment Now Available!";

                                                                        User.find({
                                                                            '_id': { $in: waitlist.users}
                                                                        }, function(err, users){
                                                                            var i;
                                                                            if (users.length > 0) {
                                                                                _.each(users, function(user){
                                                                                    MailMdl(
                                                                                        message,
                                                                                        subject,
                                                                                        user.local.email,
                                                                                        'noreply@vendorsenders.com',
                                                                                        user.local.firstName + ' ' + user.local.lastName,
                                                                                        'Vendor Senders'
                                                                                    ).send();
                                                                                });

                                                                                waitlist.remove(function(err) {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                    } else {
                                                                                        console.log('Waitlist deleted');
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    }
                                } else {
                                    //FIXME ERROR HANDLING
                                }
                            });
                        });
                    break;
                }
            });
        } else {

        }
    });
};