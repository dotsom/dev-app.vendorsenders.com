module.exports = function(
    app
) {
    // show the login form
    app.get('/forgot', function(req, res) {
        res.render('global-forgot.ejs');
    });
};