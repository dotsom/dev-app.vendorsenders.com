module.exports = function(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    ) {

    app.get('/account', function(req, res) {
        if (RouteMdl.isLoggedIn(req, res)){
            res.render('global-account.ejs', {user: req.user, uid: String(req.user._id)});
        } else {
            res.redirect('/');
        }
    });
};