module.exports = function(
    app,
// Schema
    Availability,
    Booking,
    Host,
    Waitlist,
// Models
    CalendarMdl,
    RouteMdl
) {

    var changedMonth;

    /**
     * Handle GET Requests
     */
    app.get('/dashboard', function(req, res) {
        if (!RouteMdl.isActive(req, res)) {
            res.render('global-locked.ejs');
        } else if (RouteMdl.isLoggedIn(req, res)) {
            console.log('Were active!');

            var serverData = {};
            var d = new Date();

            if (RouteMdl.isAdmin(req, res)) {
                res.render('admin-user-mgmt.ejs', {query: false, user: req.user});

            } else if (RouteMdl.isHost(req, res)) {
                Host.findById(req.user.bus.hid, function(err, host){
                    serverData.host = host;
                    Availability.find({
                        month : d.getMonth(),
                        year : d.getFullYear(),
                        hid : host._id
                    }, function(err, availabilities){
                        var cal_string = CalendarMdl.createMarkupNoPlus(d.getMonth(), d.getFullYear());
                        var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();
                        Booking.find({hid: host._id}, function(err, bookings){

                            serverData.today = d;
                            serverData.bookings = bookings;
                            serverData.avail = availabilities;
                            serverData.Calendar = CalendarMdl;
                            serverData.currentYear = d.getFullYear();
                            serverData.currentMonth = d.getMonth();
                            serverData.calendarString = cal_string;

                            var renderData = {
                                user : req.user,
                                host : host,
                                calendar : cal_string,
                                calendarHeading : cal_heading_string,
                                JSONdata : JSON.stringify(serverData),
                                serverData : serverData
                            };
                            res.render('host-dashboard.ejs', renderData);
                        });
                    });
                });
            } else {
                Host.find(function(err, hosts){
                    serverData.hosts = hosts;
                    Availability.find({
                        month : d.getMonth(),
                        year : d.getFullYear()
                    }, function(err, availabilities){
                        Booking.find({
                            uid : req.user._id
                        }, function(err, bookings){
                            Waitlist.find({
                                users : req.user._id
                            }, function(err, waitlists){
                                var cal_string = CalendarMdl.createMarkupNoPlus(d.getMonth(), d.getFullYear());
                                var cal_heading_string = CalendarMdl.monthNames[d.getMonth()] + " " + d.getFullYear();

                                serverData.today = d;
                                serverData.avails = availabilities;
                                serverData.bookings = bookings;
                                serverData.waitlists = waitlists;
                                serverData.uid = req.user._id;
                                serverData.thisUser = req.user;
                                serverData.Calendar = CalendarMdl;
                                serverData.currentYear = d.getFullYear();
                                serverData.currentMonth = d.getMonth();
                                serverData.calendarString = cal_string;

                                var renderData = {
                                    user : req.user,
                                    calendar : cal_string,
                                    calendarHeading : cal_heading_string,
                                    JSONdata : JSON.stringify(serverData),
                                    serverData : serverData
                                };
                                res.render('user-dash.ejs', renderData);
                            });
                        });
                    });
                });
            }
        } else {
            res.redirect('/');
        }
    });
};