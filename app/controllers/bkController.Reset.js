module.exports = function(
    app,
    mongoose,
    validator,
    mandrill,
    crypto,
// Schema
    User,
    PwToken,
// Models
    RouteMdl
) {

    //POST to generate a new token and send email with URL
    app.get('/reset/:token', function(req, res) {
        PwToken.findOne({token: req.params.token}, function(err, pwtoken){
            if (err) {
                res.send(err);
            } else if (!pwtoken) {
                res.send({message: 'That does not exist.'});
            } else if (pwtoken.expires <= Date.now()) {
                res.send({
                    message: 'That token has expired.',
                    expiredgt: pwtoken.expires >= Date.now(),
                    expiredlt: pwtoken.expires <= Date.now(),
                    expires: pwtoken.expires,
                    now: Date.now()
                });
            } else {
                res.render('global-reset.ejs', {
                    JSONdata: JSON.stringify({
                        uid: pwtoken.uid,
                        token: pwtoken.token
                    })
                });
            }
        });
    });
};