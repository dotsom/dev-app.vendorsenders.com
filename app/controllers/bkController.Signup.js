module.exports = function(
    app,
    validator,
//Schema
    User
) {

    // show the signup form
    app.get('/signup', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('global-signup.ejs', { message: req.flash('signupMessage') });
    });

    app.get('/signup-2', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('partials/signup-2.ejs');
    });

    app.get('/signup-3', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('partials/signup-3.ejs');
    });

    // process the signup form
    app.post('/signup-basic', function(req, res) {

        process.nextTick(function() {
            User.findOne({ 'local.email' :  req.body.email }, function(err, user) {
                // error
                if (err) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 1, err: err});
                }

                // already taken
                if (user) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 2, err: 'That email address is already in use.'});
                }

                // create user
                else {
                    // if there is no user with that email
                    // create the user
                    var newUser            = new User(),
                        errorCount         = 0,
                        errorMsgs          = [];

                    // set the user's local credentials
                    // sanitize as we go.

                    if (validator.isEmail(validator.toString(req.body.email))) {
                        newUser.local.email = String(req.body.email);
                        console.log("Valid email!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid email address.');
                    }

                    if (validator.isLength(req.body.password, 8)) {
                        newUser.local.password = newUser.generateHash(req.body.password);
                        console.log("Valid password!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Passwords must be at least 8 characters.');
                    }

                    if (req.body.firstName != "") {
                        newUser.local.firstName = String(req.body.firstName);
                        console.log("Valid first name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your first name.');
                    }

                    if (req.body.lastName != "") {
                        newUser.local.lastName = String(req.body.lastName);
                        console.log("Valid last name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your last name.');
                    }

                    if (req.body.phone != "") {
                        newUser.local.phone = String(req.body.phone);
                        console.log("Valid phone number!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a valid phone number.');
                    }

                    newUser.local.accountType       = Number(1);
                    newUser.local.accountStatus     = Number(1);

                    // save the user
                    if (errorCount == 0) {
                        console.log('New User:\n', newUser);
                        newUser.save(function(err) {
                            if (err) {
                                res.send(200, {signupErrorId: 3, err: err});
                                console.log(err);
                            } else {
                                res.send(200, {signupErrorId: false, user: newUser});
                                console.log("Successful Save!");
                            }
                        });
                    } else {
                        res.send(200, {signupErrorId: 4, err: errorMsgs});
                    }
                }
            });
        });
    });

    // process the signup form
    app.post('/signup-biz', function(req, res) {

        process.nextTick(function() {
            User.findOne({ '_id' :  req.body.uid }, function(err, user) {

                // error
                if (err) {
                    res.location('/signup');
                    res.send(200, {signupErrorId: 1, err: err});
                }

                // user exists, lets begin step 2
                if (user) {

                    var userBus = {};
                    errorCount = [];
                    console.log(user);
                    if (req.body.bname != "") {
                        userBus.name = String(req.body.bname);
                        console.log("Valid business name!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter your business name.');
                    }

                    if (req.body.binfo != "") {
                        userBus.info = String(req.body.binfo);
                        console.log("Valid business description!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please enter a business description.');
                    }

                    if (req.body.btype != "") {
                        userBus.type = String(req.body.btype);
                        console.log("Valid business type!");
                    } else {
                        errorCount++;
                        errorMsgs.push('Please select a business type.');
                    }
                    user.bus = userBus;
                    console.log(user);

                    // save the user
                    if (errorCount == 0) {
                        user.save(function(err) {
                            if (err) {
                                res.send(200, {signupErrorId: 3, err: err});
                                console.log(err);
                            } else {
                                req.logIn(user, function (err) {
                                    if(!err){
                                        res.send(200, {signupErrorId: false});
                                    }else{
                                    }
                                })
                            }
                        });
                    } else {
                        res.send(200, {signupErrorId: 4, err: errorMsgs});
                    }
                }
            });
        });
    });
};