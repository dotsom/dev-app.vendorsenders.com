var searchForAvail = function(a, key, val){
    for (var i = 0; i < a.length; i++) {
        if(a[i][key] == val) {
            return a[i];
        }
    }
    return false;
};

var Calendar = {
    monthNames  : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    createMarkup: function (month, year){
        /* days and weeks vars now ... */
        var vs_cal_running_day = new Date(year, month, 1).getDay(),
            vs_cal_days_in_month = new Date(year, month + 1, 0).getDate(),
            vs_cal_days_in_this_week = 1;
            vs_cal_day_counter = 0,
            vs_cal_dates_array = [];

            console.log(vs_cal_days_in_month);

        var vs_cal_calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

        /* table headings */
        var vs_cal_headings = ['Monday','Tuesday','Wednesday','Thursday','Friday'];
        vs_cal_calendar += '<tr class="calendar-row"><td class="calendar-day-head">';
        vs_cal_calendar += vs_cal_headings.join('</td><td class="calendar-day-head">');
        vs_cal_calendar += '</td></tr>';

        /* row for week one */
        vs_cal_calendar += '<tr class="calendar-row">';

        /* print "blank" days until the first of the current week */
        for (var vs_cal_x = 0; vs_cal_x < vs_cal_running_day; vs_cal_x++) {
            if (vs_cal_days_in_this_week > 1 && vs_cal_days_in_this_week < 7){
                vs_cal_calendar += '<td class="calendar-day-np"> </td>';
            }
            vs_cal_days_in_this_week++;
        }

        /* keep going with days.... */
        for (var vs_cal_list_day = 1; vs_cal_list_day <= vs_cal_days_in_month; vs_cal_list_day++) {
            if (vs_cal_days_in_this_week > 1 && vs_cal_days_in_this_week < 7){
                vs_cal_calendar += '<td data-day="' + vs_cal_list_day + '" class="calendar-day">';
                vs_cal_calendar += '<div class="day-number">' + vs_cal_list_day + '</div>';
                vs_cal_calendar += '<div class="day-icon-bar i2">';
                vs_cal_calendar += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';
                vs_cal_calendar+= '</td>';
            }

            if (vs_cal_running_day == 6) {
                vs_cal_calendar+= '</tr>';
                if((vs_cal_day_counter+1) != vs_cal_days_in_month){
                    vs_cal_calendar+= '<tr class="calendar-row">';
                }
                vs_cal_running_day = -1;
                vs_cal_days_in_this_week = 0;
            }
            vs_cal_days_in_this_week++;
            vs_cal_running_day++;
            vs_cal_day_counter++;
        }

        /* finish the rest of the days in the week */
        if (vs_cal_days_in_this_week < 8) {
            for(var vs_cal_x = 1; vs_cal_x <= (7 - vs_cal_days_in_this_week); vs_cal_x++) {
                vs_cal_calendar+= '<td class="calendar-day-np"> </td>';
            }
        }

        /* final row */
        vs_cal_calendar += '</tr>';

        /* end the table */
        vs_cal_calendar += '</table>';

        /* all done, return result */
        return vs_cal_calendar;
    },
    createMarkupNoPlus: function (month, year){
        var vs_cal_calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

        /* table headings */
        var vs_cal_headings = ['Monday','Tuesday','Wednesday','Thursday','Friday'];
        vs_cal_calendar += '<tr class="calendar-row"><td class="calendar-day-head">';
        vs_cal_calendar += vs_cal_headings.join('</td><td class="calendar-day-head">');
        vs_cal_calendar += '</td></tr>';

        /* days and weeks vars now ... */
        var vs_cal_running_day = new Date(year, month, 1).getDay(),
            vs_cal_days_in_month = new Date(year, month + 1, 0).getDate(),
            vs_cal_days_in_this_week = 1;
            vs_cal_day_counter = 0,
            vs_cal_dates_array = [];

            console.log(vs_cal_days_in_month);


        /* row for week one */
        vs_cal_calendar += '<tr class="calendar-row">';

        /* print "blank" days until the first of the current week */
        for (var vs_cal_x = 0; vs_cal_x < vs_cal_running_day; vs_cal_x++) {
            if (vs_cal_days_in_this_week > 1 && vs_cal_days_in_this_week < 7){
                vs_cal_calendar += '<td class="calendar-day-np"> </td>';
            }
            vs_cal_days_in_this_week++;
        }

        /* keep going with days.... */
        for (var vs_cal_list_day = 1; vs_cal_list_day <= vs_cal_days_in_month; vs_cal_list_day++) {
            if (vs_cal_days_in_this_week > 1 && vs_cal_days_in_this_week < 7){
                vs_cal_calendar += '<td data-day="' + vs_cal_list_day + '" class="calendar-day">';
                vs_cal_calendar += '<div class="day-number">' + vs_cal_list_day + '</div>';
                vs_cal_calendar+= '</td>';
            }

            if (vs_cal_running_day == 6) {
                vs_cal_calendar+= '</tr>';
                if((vs_cal_day_counter+1) != vs_cal_days_in_month){
                    vs_cal_calendar+= '<tr class="calendar-row">';
                }
                vs_cal_running_day = -1;
                vs_cal_days_in_this_week = 0;
            }
            vs_cal_days_in_this_week++;
            vs_cal_running_day++;
            vs_cal_day_counter++;
        }

        /* finish the rest of the days in the week */
        if (vs_cal_days_in_this_week < 8) {
            for(var vs_cal_x = 1; vs_cal_x <= (7 - vs_cal_days_in_this_week); vs_cal_x++) {
                vs_cal_calendar+= '<td class="calendar-day-np"> </td>';
            }
        }

        /* final row */
        vs_cal_calendar += '</tr>';

        /* end the table */
        vs_cal_calendar += '</table>';

        /* all done, return result */
        return vs_cal_calendar;
    },
    changeMonthArrow : function (month, year, dir, plus){

        plus = typeof plus !== 'undefined' ? plus : true;

        if (dir === "prev") {
            if (month > 0) {
                month--;
            } else {
                month = 11;
                year--;
            }
        } else {
            if (month < 11) {
                month++;
            } else {
                month = 0;
                year++;
            }
        }
        var returns = {};
        if (plus) {
            returns.newCalendar = Calendar.createMarkup(month, year);
        } else {
            returns.newCalendar = Calendar.createMarkupNoPlus(month, year);
        }
        returns.newMonth = month;
        returns.newYear = year;

        return returns;
    },
    changeMonth : function (month, year, plus){

        plus = typeof plus !== 'undefined' ? plus : true;

        var returns = {};
        if (plus) {
            returns.newCalendar = Calendar.createMarkup(month, year);
        } else {
            returns.newCalendar = Calendar.createMarkupNoPlus(month, year);
        }
        returns.newMonth = month;
        returns.newYear = year;

        return returns;
    },
};

module.exports = Calendar;