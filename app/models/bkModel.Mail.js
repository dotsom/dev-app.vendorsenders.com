MailMdl = function (message, subject, toEmail, fromEmail, toName, fromName){
    var newMail = {
        mandrill : require('node-mandrill')('7BzcCNailMlLHtGQ8xkheQ'),
        email : {
            "key": "7BzcCNailMlLHtGQ8xkheQ",
            "message": {
                "html": message,
                "subject": subject,
                "from_email": fromEmail,
                "from_name": fromName,
                "to": [
                    {
                        "email": toEmail,
                        "name": toName,
                        "type": "to"
                    }
                ],
                "headers": {
                    "Reply-To": "message.noreply@vendorsenders.com"
                }
            },
            "async": false,
            "ip_pool": "Main Pool"
        },
        send : function(){
            this.mandrill('/messages/send', this.email, function(err, m_res){
                if (err) {
                    console.log(err);
                } else {
                    console.log('email sent');
                }
            });
        }
    };

    return newMail;
};

module.exports = MailMdl;