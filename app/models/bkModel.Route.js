var RouteMdl = {};
RouteMdl.isLoggedIn = function (req, res) {
    if (req.isAuthenticated()) {
        return true;
    } else {
        return false;
    }
};

RouteMdl.isActive = function (req, res) {
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        if (req.user.local.accountStatus == 2) {
            return true;
        } else {
            return false;
        }
    }
};

RouteMdl.isThisUser = function (req, uid) {
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        /*if (req.user._id == uid) {
            return true;
        } else {
            return false;
        }*/
        console.log(req.user._id);
        console.log(uid);
        return true;
    }
};

RouteMdl.isAdmin = function (req, res){
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        if (req.user.local.accountType == 99) {
            return true;
        } else {
            return false;
        }
    }
};

RouteMdl.isHost = function (req, res){
    if (typeof req.user == 'undefined') {
        return false;
    } else {
        if (req.user.local.accountType == 2) {
            return true;
        } else {
            return false;
        }
    }
};

module.exports = RouteMdl;