module.exports = function(
    app,
    passport,
    localStrategy,
    bcrypt,
    validator,
    mongoose,
    _,
    extend,
    encoder,
    mandrill,
    crypto,
    appRoot,
// Schema
    Availability,
    Booking,
    Host,
    Notification,
    Waitlist,
    User,
    PwToken,
    File,
// Models
    CalendarMdl,
    RouteMdl,
    MailMdl
) {

//MODEL CONTROLLERS
    //PASSPORT
    require('./controllers/bkController.Passport')(
        passport,
        localStrategy,
        bcrypt,
    // Schema
        User,
        Host
    );

    // CALENDAR
    require('./controllers/bkController.Calendar')(
        app,
        _,
    // Schema
        Host,
        Availability,
        Booking,
        User,
        Waitlist,
    // Models
        CalendarMdl,
        RouteMdl,
        MailMdl
    );

    // USER API
    require('./controllers/bkController.API.User')(
        app,
        passport,
        mongoose,
        validator,
        _,
        encoder,
        mandrill,
    // Schema
        Availability,
        Booking,
        File,
        Host,
        User,
        Waitlist,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // HOST API
    require('./controllers/bkController.API.Host')(
        app,
        passport,
        mongoose,
        validator,
        extend,
    // Schema
        Availability,
        Booking,
        File,
        Host,
        User,
        Waitlist,
    // Models
        RouteMdl
    );

    // FILE API
    require('./controllers/bkController.API.File')(
        app,
        passport,
        mongoose,
        validator,
        appRoot,
    // Schema
        Host,
        User,
        File,
    // Models
        RouteMdl
    );

    // NOTIFICATION API
    require('./controllers/bkController.API.Notification')(
        app,
        passport,
        mongoose,
        validator,
        extend,
        _,
    // Schema
        Notification,
    // Models
        RouteMdl
    );

    // AVAILABILITY API
    require('./controllers/bkController.API.Availability')(
        app,
        passport,
        mongoose,
        _,
        Availability,
        Host
    );

    // BOOKING API
    require('./controllers/bkController.API.Booking')(
        app,
        passport,
        mongoose,
        Booking,
        MailMdl
    );

    // WAITLIST API
    require('./controllers/bkController.API.Waitlist')(
        app,
        passport,
        mongoose,
        validator,
        _,
        encoder,
    // Schema
        Availability,
        Booking,
        Host,
        Waitlist,
        User,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // FORGOT PW TOKEN GENERATION API
    require('./controllers/bkController.API.PwToken')(
        app,
        mongoose,
        validator,
        mandrill,
        crypto,
    // Schema
        User,
        PwToken,
    // Models
        RouteMdl
    );


//VIEW CONTROLLERS
    // HOME
    require('./controllers/bkController.Home')(app);

    require('./controllers/bkController.Test')(app);

    // SIGNUP
    require('./controllers/bkController.Signup')(
        app,
        validator,
    //Schema
        User
    );

    // LOGIN
    require('./controllers/bkController.Login')(
        app,
        passport,
    //Schema
        User
    );


    // FORGOT
    require('./controllers/bkController.Forgot')(
        app
    );

    // RESET
    require('./controllers/bkController.Reset')(
        app,
        mongoose,
        validator,
        mandrill,
        crypto,
    // Schema
        User,
        PwToken,
    // Models
        RouteMdl
    );

    // LOGOUT
    require('./controllers/bkController.Logout')(app);

    // ADMIN: NEW HOST
    require('./controllers/bkController.AdminNewHost')(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    );

    // ADMIN: USER MANAGEMENT
    require('./controllers/bkController.AdminUserMgmt')(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    );

    // ADMIN: HOST MANAGEMENT
    require('./controllers/bkController.AdminHostMgmt')(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    );

    // ADMIN: USER CALENDAR
    require('./controllers/bkController.AdminUserCalendar')(
        app,
        passport,
        mongoose,
        validator,
        _,
        encoder,
    // Schema
        Availability,
        Booking,
        Host,
        User,
        Waitlist,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // ADMIN: HOST CALENDAR
    require('./controllers/bkController.AdminHostCalendar')(
        app,
        passport,
        mongoose,
        validator,
        _,
        encoder,
    // Schema
        Availability,
        Booking,
        Host,
        User,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // ADMIN: SET AVAILABILITIES
    require('./controllers/bkController.AdminSetAvailability')(
        app,
    // Schema
        Availability,
        Host,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // ADMIN AND USER: DASHBOARD
    require('./controllers/bkController.GlobalDash')(
        app,
    // Schema
        Availability,
        Booking,
        Host,
        Waitlist,
    // Models
        CalendarMdl,
        RouteMdl
    );

    // GLOBAL: ACCOUNT
    require('./controllers/bkController.GlobalAccount')(
        app,
        validator,
    // Schema
        Host,
        User,
    // Models
        RouteMdl
    );
};


