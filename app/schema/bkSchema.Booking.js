// app/models/bookingModel.js
var mongoose = require('mongoose');
var bookingSchema = mongoose.Schema({
    hid : { type: String, required: true, trim: true },
    uid : { type: String, required: true, trim: true },
    aid : { type: String, required: true, trim: true },
});
module.exports = mongoose.model('Booking', bookingSchema, 'bookings');