// app/models/HostModel.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var hostSchema = mongoose.Schema({
  profile           :
  { email           : { type: String, required: true, trim: true }
  , password        : { type: String, required: true, trim: true }
  , accountStatus   : { type: Number, required: true, trim: true }
  , profileImage    :
    { filetype                : { type: String, required: false, trim: true }
    , name                    : { type: String, required: false, trim: true }
    , fullPath                : { type: String, required: false, trim: true }}}
, company           :
  { name            : { type: String, required: true, trim: true }
  , address         : { type: String, required: true, trim: true }
  , city            : { type: String, required: true, trim: true }
  , state           : { type: String, required: true, trim: true }
  , zip             : { type: String, required: true, trim: true }
  , phone           : { type: String, required: true, trim: true }
  , size            : { type: Number }
  , fee             : { type: Number }}
, restrictions      :
  { max             : { type: Number }
  , items           : { type: String, required: false}}
});

// methods ======================
// generating a hash
hostSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
hostSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// checking if password is valid
hostSchema.methods.isMatchingPassword = function(password, cpassword) {
    return cpassword == password;
};

// create the model for users and expose it to our app
module.exports = mongoose.model('Host', hostSchema);