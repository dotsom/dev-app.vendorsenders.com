var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = Schema({
    uid : [],
    message : { type: String, required: true, trim: true },
    meta: {},
    messageType : { type: String, required: true, trim: true },
    read : {},
    timeRead : { type: Schema.Types.Mixed, required: true, default: {}},
    timeCreated : { type: Date, required: true, default: Date.now }
});
module.exports = mongoose.model('Notification', notificationSchema);