// app/schema/bkSchema.Waitlist.js

var mongoose = require('mongoose');

var waitlistSchema = mongoose.Schema({
    hid : { type: String, required: true, trim: true },
    aid : { type: String, required: true, trim: true },
    users : [String]

});

// create the model for users and expose it to our app
module.exports = mongoose.model('Waitlist', waitlistSchema, 'waitlists');