var express             = require('express')
,   app                 = express()
,   port                = process.env.PORT || 8080
,   mongoose            = require('mongoose')
,   passport            = require('passport')
,   localStrategy       = require('passport-local').Strategy
,   flash               = require('connect-flash')
,   configDB            = require('./config/database.js')
,   bcrypt              = require('bcrypt-nodejs')
,   validator           = require('validator')
,   _                   = require('underscore')
,   Encoder             = require('node-html-encoder').Encoder
,   encoder             = new Encoder('entity')
,   mandrill            = require('node-mandrill')('7BzcCNailMlLHtGQ8xkheQ')
,   crypto              = require('crypto')
,   extend              = require('extend')
,   path                = require('path')
,   appRoot             = path.resolve(__dirname)
// Schema
,   Availability        = require('./app/schema/bkSchema.Availability')
,   Booking             = require('./app/schema/bkSchema.Booking')
,   Host                = require('./app/schema/bkSchema.Host')
,   Notification        = require('./app/schema/bkSchema.Notification')
,   Waitlist            = require('./app/schema/bkSchema.Waitlist')
,   User                = require('./app/schema/bkSchema.User')
,   PwToken             = require('./app/schema/bkSchema.PwToken')
,   File                = require('./app/schema/bkSchema.File')

// Models
,   CalendarMdl         = require('./app/models/bkModel.Calendar')
,   RouteMdl            = require('./app/models/bkModel.Route')
,   MailMdl             = require('./app/models/bkModel.Mail');

//Connect to DB
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('Successful connection to mongodb at ' + configDB.url);
});

app.configure(function() {
    app.use(express.logger('dev')); // log every request to the console
    app.use(express.cookieParser()); // read cookies (needed for auth)
    app.use(express.bodyParser()); // get information from html forms
    app.set('view engine', 'ejs'); // set up ejs for templating
    app.set('views', 'public/views');
    app.use(express.static('public'));
    // required for passport
    // session secret
    app.use(express.session({ secret: 'cats on the keyboard!' }));
    app.use(passport.initialize());
    // persistent login sessions
    app.use(passport.session());
    // use connect-flash for flash messages stored in session
    app.use(flash());
});

// Routes
require('./app/routes.js')(
    app,
    passport,
    localStrategy,
    bcrypt,
    validator,
    mongoose,
    _,
    extend,
    encoder,
    mandrill,
    crypto,
    appRoot,
// Schema
    Availability,
    Booking,
    Host,
    Notification,
    Waitlist,
    User,
    PwToken,
    File,
// Models
    CalendarMdl,
    RouteMdl,
    MailMdl
);

// Launch
app.listen(port);
console.log('Listening on port ' + port);