$(document).ready(function() {
    var availableDayMarkup, $calendarCell, currentHID;
    var initArgs = {
        situ : 'pageload-host',
        avail : serverData.avail,
        bookings : serverData.bookings
    };
    calendarMdl.processCalendarView(initArgs);

    $('.changeMonth').click(function(){
        var dir = $(this).attr('data-direction');
        var changeMonthArgs = {
          month : serverData.currentMonth,
          year : serverData.currentYear,
          dir : dir,
          situ : 'pageload-host',
          plus : false,
          hostProfile : true
        };
        calendarMdl.changeMonthArrow(changeMonthArgs);
    });
});