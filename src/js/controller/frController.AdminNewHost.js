$(document).ready(function() {

    function buildHostReview(data) {
        var markup = "<div class=\"row\"><div class=\"col-sm-6\"><div class=\"well\"><h3>New Company Created -" + data.host.company.name + "</h3><p><strong>email</strong>:" + data.host.profile.email + "<br><strong>address</strong>:" + data.host.company.address + "<br><strong>phone number</strong>:" + data.host.company.phone + "<br><strong>city</strong>:" + data.host.company.city + "<br><strong>state</strong>:" + data.host.company.state + "<br><strong>zip</strong>:" + data.host.company.zip + "<br></p></div></div></div>";

        return markup;
    }

    function transitionForm(markup){
        $('#newHostWrap').addClass('reloading')
            .delay(700)
            .html(markup).removeClass('reloading');
    }

    // FORMAT AND SUBMIT VIA AJAX
    function submitNewHost() {
        // DEFINE
        var companyInfo = {},
            newNotification = {};

        companyInfo.email = $("input#cEmail").val();
        companyInfo.password = $("input#cPassword").val();
        companyInfo.name = $("input#cName").val();
        companyInfo.phoneDirty = $("input#cPhone").val();
        companyInfo.phone = companyInfo.phoneDirty.replace(/\D/g,'');
        companyInfo.address1 = $("input#cAddress1").val();
        companyInfo.city = $("input#cCity").val();
        companyInfo.state = $("select#cState").val();
        companyInfo.zip = $("input#cZip").val();
        companyInfo.fee = $("input#cFee").val();
        companyInfo.max = $("input#cMax").val();
        companyInfo.items = $("textarea#cRestrictions").val();
        companyInfo.size = $("select#cSize").val();

        newNotification.message = companyInfo.name + " has joined VendorSenders!  Keep an eye out for new booking opportunities.";
        newNotification.messageType = 1;

        vs.api.host.newHost(companyInfo, function(){
            vs.api.user.getUidsByType('1', function(uids){

                newNotification.uid = uids;
                vs.api.notification.newNotification(newNotification, function(){
                    window.location.href = '/admin/host-mgmt';
                });
            });
        });
    }

    // BOOTSTRAP VALIDATION
    function validateNewHost() {
        $('#newHostForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cEmail: {
                    validators: {
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                },
                cPassword: {
                    validators: {
                        notEmpty: {
                            message: 'A password is required and cannot be empty'
                        },
                        stringLength: {
                            min: 8,
                            message: 'Your password must be at least 8 characters'
                        }
                    }
                },
                cCPassword: {
                    validators: {
                        notEmpty: {
                            message: 'You must confirm your password'
                        },
                        identical: {
                            field: 'cPassword',
                            message: 'Your passwords do not match'
                        }
                    }
                },
                cName: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a company name'
                        }
                    }
                },
                cPhone: {
                    validators: {
                        phone: {
                            message: 'This is not a valid phone number'
                        }
                    }
                },
                cAddress1: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide an address'
                        }
                    }
                },
                cCity: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a city'
                        }
                    }
                },
                cState: {
                    validators: {
                        notEmpty: {
                            message: 'You must select a state'
                        }
                    }
                },
                cZip: {
                    validators: {
                        zipCode: {
                            message: 'You must provide a zip code'
                        }
                    }
                },
                cSize: {
                    validators: {
                        notEmpty: {
                            message: 'You must specify a company size'
                        }
                    }
                },
                cFee: {
                    validators: {
                        numeric: {
                            message: 'Enter the fee as either an integer or a decimal number.  Do not use a \'$\''
                        }
                    }
                },
                cMax: {
                    validators: {
                        numeric: {
                            message: 'Enter the max number of days per year as a whole number'
                        }
                    }
                },
            }
        });
    }
    validateNewHost();
    $("form#newHostForm").unbind('submit');
    $("form#newHostForm").submit(function(event) {

        $('#signupButton').attr('disabled', 'disabled');

        // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
        event.preventDefault();
        event.stopImmediatePropagation();
        submitNewHost();
        return false;
    });
});