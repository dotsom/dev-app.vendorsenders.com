$(document).ready(function() {
    var availableDayMarkup, $calendarCell, currentHID;
    var initArgs = {
      avail : serverData.avails,
      booking : serverData.bookings,
      waitlist : serverData.waitlists,
      situ : 'pageload-user-admin'
    };
    calendarMdl.processCalendarView(initArgs);

    $('.changeMonth').click(function(){
        var dir = $(this).attr('data-direction');
        var changeMonthArgs = {
          month : serverData.currentMonth,
          year : serverData.currentYear,
          dir : dir,
          situ : 'pageload-user-admin',
          plus : false
        };
        calendarMdl.changeMonthArrow(changeMonthArgs);
    });
});