$(document).ready(function(){
  var logoFromTop = $(window).scrollTop();

  $(window).scroll(function(e){
    logoFromTop = $(this).scrollTop();
    imgHeight = $('#logo svg').height();

    if (logoFromTop <= imgHeight) {
      $('#logo svg').css({
        clip: 'rect(0px, 255px, ' + (imgHeight - logoFromTop) + 'px, 0px)'
      });
    } else {
      $('#logo svg').css({
        clip: 'rect(0px, 255px,  0px, 0px)'
      });
    }
  });
 $('.parent-label').click(function(e){
  e.preventDefault();
  $(this).find('.more').toggleClass('turnt');
  $(this).siblings('.child-list').toggleClass('hiding');
 });

 $('.contact-modal').click(function(e){
  e.preventDefault();
  var markup = calendarMdl.createModalMarkup('contact');
  $('body').prepend(markup);
  calendarMdl.bindExitX();
 });
});
