var userNotifications;

function notificationTray(el, uid){
  vs.api.notification.getByUid(uid, function(notifications){

    // Variables
    var count = 0;
    var markup = "<ul class='hidden' id='notification-list'>";

    userNotifications = notifications;

    // What happens when it loads
    for (var i = 0; i < userNotifications.length; i++) {
      if (i < 5) {
        markup += '<li class="notification-item';

        if (userNotifications[i]['read'][uid] == false) {
          count++;
          markup += ' unread';
        }

        markup += '" >';

        switch (userNotifications[i].messageType) {
          case "1":
            markup += '<i class="notification-icon fa fa-building"></i>';
            break;

          default:
            break;
        }

        markup += _.escape(userNotifications[i].message)
        markup += '</li>';
      }
    }
    if (count) {
      $(el).find('#notification-icon').append('<span id="notification-count">' + count + '</span>');
    }
    $(el).append(markup);

    // What happens when you click it
    $(el).click(function(e){
      $('#notification-list').toggleClass('hidden');
    });

  });
}