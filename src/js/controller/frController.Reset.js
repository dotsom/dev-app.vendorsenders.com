$(document).ready(function() {

    // BOOTSTRAP VALIDATION PART 1
    function validatePassword() {
        $('#reset_pw_form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: 'A password is required and cannot be empty'
                        },
                        stringLength: {
                            min: 8,
                            message: 'Your password must be at least 8 characters'
                        }
                    }
                },
                cpassword: {
                    validators: {
                        notEmpty: {
                            message: 'You must confirm your password'
                        },
                        identical: {
                            field: 'password',
                            message: 'Your passwords do not match'
                        }
                    }
                }
            }
        });
    }

    // FORMAT AND SUBMIT THE FIRST PART VIA AJAX
    function submitForm() {
        // DEFINE
        var password = $("input#password").val();

        // BUILD DATA OBJECT
        var data = {
            password:  password
        };

        // SEND POST REQUEST WITH DATA OBJECT
        $.ajax({
            type : 'PUT',
            url : '/api/users/' + serverData.uid,
            data : data,
            dataType : 'json',
            encode : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            $.ajax({
                type : 'DELETE',
                url : '/api/pwtoken/' + serverData.token,
                dataType : 'json',
                encode : true
            }).done(function(data) {
                window.location.assign('/login');
            });
        });
    }

    // CALL THE FIRST VALIDATE ON LOAD
    validatePassword();

    $("form#reset_pw_form").unbind('submit');
    $("form#reset_pw_form").submit(function(event) {
        $('#signupButton').attr('disabled', 'disabled');

        // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
        event.preventDefault();
        event.stopImmediatePropagation();
        submitForm();
        return false;
    });
});