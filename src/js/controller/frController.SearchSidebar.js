var searchSidebar = {},
    geocoder = new google.maps.Geocoder();

(function($, searchSidebar){
  $.fn.searchFilterIcons = function(handlers){

    // Create empty elements to house filters and results
    $(this).parent().append('<div id="filterWrap"></div><ul id="resultsWrap"></ul>');

    // Store all icons and wrappers for later use
    var $icons = $('.search-method-select');
      $iconSet = $('#iconWrap'),
      $filterWrap = $('#filterWrap'),
      $resultsWrap = $('#resultsWrap');

    // Bind icon switching
    $icons.click(function(e){
      var $this = $(this),
          $thisAttr = $this.attr('filter-method');
      $icons.removeClass('selected');
      $(this).addClass('selected');
      $iconSet.attr('current-filter', $thisAttr).trigger('search-filter-change');
      e.preventDefault();
    });

    // Listen for filter change, provide correct filter
    $iconSet.on('search-filter-change', function(){
      var thisFilter = $(this).attr('current-filter');
      $filterWrap.html(reference.filterMarkup[thisFilter]);
      handlers[thisFilter]();
    });
  }

  $.fn.resetSearchFilterIcons = function(handlers){
    // Store all icons and wrappers for later use
    var $icons = $('.search-method-select');
      $iconSet = $('#iconWrap'),
      $filterWrap = $('#filterWrap'),
      $resultsWrap = $('#resultsWrap');

    $icons.unbind('click');
    $iconSet.unbind('search-filter-change');

    // Bind icon switching
    $icons.click(function(e){
      var $this = $(this),
          $thisAttr = $this.attr('filter-method');
      $icons.removeClass('selected');
      $(this).addClass('selected');
      $iconSet.attr('current-filter', $thisAttr).trigger('search-filter-change');
      e.preventDefault();
    });

    // Listen for filter change, provide correct filter
    $iconSet.on('search-filter-change', function(){
      var thisFilter = $(this).attr('current-filter');
      $filterWrap.html(reference.filterMarkup[thisFilter]);
      handlers[thisFilter]();
    });
  }

  searchSidebar.handleAlphabetical = function(hosts){
    var newMarkup = '';

    for (var i = 0; i < hosts.length; i++) {
      newMarkup += '<li data-hid="' + hosts[i]._id + '">';
      newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
      newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
      newMarkup += '</li>';
    }

    $('#resultsWrap').html(newMarkup);

    if(!$('#resultsWrap').hasClass('has-content')){
      $('#resultsWrap').addClass('has-content')
    }

    calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
      args2.args1 = args1;
      calendarMdl.processSidebarView(args2);
    });
  };

  searchSidebar.bindNameSearch = function(hosts){
    $('#name-search').on('input propertychange paste', function(){
      searchSidebar.handleNameSearch($(this).children('input').val(), hosts);
    });
  };

  searchSidebar.handleNameSearch = function(str, hosts){
    if (str == "") {
      return false;
    } else {
      var newMarkup = '',
      count = 0;

      for (var i = 0; i < hosts.length; i++) {
        if (hosts[i].company.name.match(new RegExp(str.toLowerCase(), 'i'))) {
          count ++;
          newMarkup += '<li data-hid="' + hosts[i]._id + '">';
          newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
          newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
          newMarkup += '</li>';
        }
      }

      if (count < 1) {
        newMarkup = '<li>No results for that search</li>';
      }

      $('#resultsWrap').html(newMarkup);
      if(!$('#resultsWrap').hasClass('has-content')){
        $('#resultsWrap').addClass('has-content')
      }
      calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
        args2.args1 = args1;
        calendarMdl.processSidebarView(args2);
      });
    }
  };

  searchSidebar.handleFeeSearch = function(hosts) {
    var newMarkup = '',
    count = 0;

    for (var i = 0; i < hosts.length; i++) {
      count++;
      newMarkup += '<li data-hid="' + hosts[i]._id + '">';
      newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
      newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
      newMarkup += '</li>';
    }

    if (count < 1) {
      newMarkup = '<li>No results for that search</li>';
    }

    $('#resultsWrap').html(newMarkup);
    if(!$('#resultsWrap').hasClass('has-content')){
      $('#resultsWrap').addClass('has-content')
    }

    calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
      args2.args1 = args1;
      calendarMdl.processSidebarView(args2);
    });
  };

  searchSidebar.bindSizeSearch = function(hosts){
    $('#size-search select').change(function(e){
      e.preventDefault;
      var sizeCat = parseInt($(this).children('option:selected').val());
      searchSidebar.handleSizeSearch(sizeCat, hosts);
    });
  };

  searchSidebar.handleSizeSearch = function(range, hosts) {
    var newMarkup = '',
    count = 0;
    for (var i = 0; i < hosts.length; i++) {
      if (hosts[i].company.size === range) {
        count++;
        newMarkup += '<li data-hid="' + hosts[i]._id + '">';
        newMarkup += '<h3 class="view-host-avail">' + hosts[i].company.name + '</h3>';
        newMarkup += '<span class="host-info">$' + hosts[i].company.fee + ' | ' + hosts[i].company.city + ', ' + hosts[i].company.state + '</span>';
        newMarkup += '</li>';
      }
    }
    if (count < 1) {
      newMarkup = '<li>No results for that search</li>';
    }
    $('#resultsWrap').html(newMarkup);
    if(!$('#resultsWrap').hasClass('has-content')){
      $('#resultsWrap').addClass('has-content')
    }
    calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
      args2.args1 = args1;
      calendarMdl.processSidebarView(args2);
    });
  };

  searchSidebar.bindZipSearch = function(hosts){
    var $zipWrap = $('#zip-search');

    $zipWrap.find('span.bttn.location')
      .unbind('click')
      .click(function(e){
        e.preventDefault();
        e.stopPropagation();
        searchSidebar.handleZipSearch($zipWrap.children('input').val(), hosts);
    });
  };

  searchSidebar.handleZipSearch = function(zip, hosts){
    var lat, lng;
    geocoder.geocode({address: zip}, function(results, status){
      var newMarkup = '',
      count = 0;

      if (status == google.maps.GeocoderStatus.OK){

          var here = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          };

          for (var j = 0; j < hosts.length; j++) {
            hosts[j].company.distance = reference.distance(
              hosts[j].company.lat,
              hosts[j].company.lng,
              here.lat,
              here.lng
            );
          }

          hosts.sort(reference.compareDistance);

          for (var j = 0; j < hosts.length; j++) {

            count ++;
            newMarkup += '<li data-hid="' + hosts[j]._id + '">';
            newMarkup += '<h3 class="view-host-avail">' + hosts[j].company.name + '</h3>';
            newMarkup += '<span class="host-info">' + ( hosts[j].company.distance > 1 ? Math.round(hosts[j].company.distance) : hosts[j].company.distance.toFixed(1)) + ' miles | ' + hosts[j].company.city + ', ' + hosts[j].company.state + '</span>';
            newMarkup += '</li>';
          }

          if (count < 1) {
            newMarkup = '<li>No results for that search</li>';
          }

          $('#resultsWrap').html(newMarkup);
          if(!$('#resultsWrap').hasClass('has-content')){
            $('#resultsWrap').addClass('has-content')
          }
          calendarMdl.bindSideHostAvail({avail: serverData.avails}, function(args1, args2){
            args2.args1 = args1;
            calendarMdl.processSidebarView(args2);
          });

      } else {
      }
    });
  };
})(jQuery, searchSidebar);

$(document).ready(function() {
  // Execution
    var locations = [];
    for (var i = 0; i < serverData.hosts.length; i++) {

      (function(i, hosts){

        hosts[i].company.address = hosts[i].company.address + ', '
          + hosts[i].company.city + ', '
          + hosts[i].company.state + ' '
          + hosts[i].company.zip;

        geocoder.geocode({
          address: hosts[i].company.address
        },function(results, status){
          if (status == google.maps.GeocoderStatus.OK){

            locations.push([results[0].geometry.location.lat(), results[0].geometry.location.lng()]);
            hosts[i].company.lat = results[0].geometry.location.lat();
            hosts[i].company.lng = results[0].geometry.location.lng();

            if (hosts.length === locations.length){
              $('#iconWrap').searchFilterIcons({
                name: function(){
                  searchSidebar.bindNameSearch(hosts)
                },
                fee: function(){
                  searchSidebar.handleFeeSearch(hosts)
                },
                zip: function(){
                  searchSidebar.bindZipSearch(hosts)
                },
                size: function(){
                  searchSidebar.bindSizeSearch(hosts)
                }
              });
              // List alphabetical by default
              searchSidebar.handleAlphabetical(hosts);
            }
          } else {
          }
        });
      })(i, serverData.hosts);
    }
});
