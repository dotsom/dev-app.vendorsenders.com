$(document).ready(function() {

    // BOOTSTRAP VALIDATION PART 1
    function validateSignup1() {
        $('#signupForm1').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'A password is required and cannot be empty'
                        },
                        stringLength: {
                            min: 8,
                            message: 'Your password must be at least 8 characters'
                        }
                    }
                },
                cpassword: {
                    validators: {
                        notEmpty: {
                            message: 'You must confirm your password'
                        },
                        identical: {
                            field: 'password',
                            message: 'Your passwords do not match'
                        }
                    }
                },
                firstName: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a first name'
                        }
                    }
                },
                lastName: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a last name'
                        }
                    }
                },
                phone: {
                    validators: {
                        phone: {
                            message: 'This is not a valid phone number'
                        }
                    }
                },
                contactMethod: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a preferred method of contact'
                        }
                    }
                }
            }
        }).on('success.field.fv', function(e, data) {
            if (data.fv.isValid()) {
                data.fv.disableSubmitButtons(false);
            }
        });
    }

   function validateSignup2() {
        $('#signupForm2').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                bname: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a business name'
                        }
                    }
                },
                binfo: {
                    validators: {
                        notEmpty: {
                            message: 'You must provide a description of your business'
                        }
                    }
                },
                btype: {
                    validators: {
                        notEmpty: {
                            message: 'You have to tell us what you sell'
                        }
                    }
                }
            }
        });
    }

   function validateSignup3() {
        $('#signupForm3').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'accept[]': {
                    validators: {
                        choice: {
                            min: 1,
                            message: 'You must accept the Terms of Service to create your account'
                        }
                    }
                }
            }
        });
    }

    // RETRIEVE THE SECOND PART OF THE FORM AND ANIMATE THE TRANSITION
    function transitionForm1(user){
        $('#signupWrap').addClass('reloading');

        $.ajax({
            type : 'GET',
            url : '/signup-2',
            dataType : 'html',
        }).done(function(data) {
            $('#signupWrap').delay(700).html(data).removeClass('reloading');
            validateSignup2();
            $("form#signupForm2").unbind('submit');
            $("form#signupForm2").submit(function(event) {
                event.preventDefault();
                event.stopImmediatePropagation();
                if ($('.has-success').length == 3) {
                    $('#signupButton').attr('disabled', 'disabled');
                    collectForm2(user);
                    return false;
                }
            });
        });
    }

    // RETRIEVE THE THIRD PART OF THE FORM AND ANIMATE THE TRANSITION
    function transitionForm2(user){
        $('#signupWrap').addClass('reloading');

        $.ajax({
            type : 'GET',
            url : '/signup-3',
            dataType : 'html',
        }).done(function(data) {
            $('#signupWrap').html(data).delay(700).removeClass('reloading');
            validateSignup3();
            $("form#signupForm3").unbind('submit');
            $("form#signupForm3").submit(function(event) {
                event.preventDefault();
                event.stopImmediatePropagation();
                $('#signupButton').attr('disabled', 'disabled');
                submitForm(user);
                return false;
            });
        });
    }

    // FORMAT AND STORE
    function collectForm1() {
        // DEFINE
        var email = $("input#email").val(),
            password = $("input#password").val(),
            firstName = $("input#firstName").val(),
            lastName = $("input#lastName").val(),
            phone = $("input#phone").val(),
            phoneClean = phone.replace(/\D/g,''),
            accountType = $("input#accountType").val(),
            contactMethod = $("select#contactMethod").val();
            paymentMethod = $("select#paymentMethod").val();

        // BUILD DATA OBJECT
        var newUser = {
            local: {
                email:  email,
                password: password,
                firstName: firstName,
                lastName: lastName,
                phone: phoneClean,
                accountType: accountType,
                contactMethod: contactMethod,
                paymentMethod: paymentMethod
            }
        };

        transitionForm1(newUser);
    }

    // FORMAT AND STORE
    function collectForm2(user) {

        var typeIsOther = ($("input[name=btype]:checked").val() == '19') ? true : false;
        // DEFINE
        var bname = $("input#bname").val(),
            binfo = $("textarea#binfo").val(),
            btype = typeIsOther ? $("input#btype-oth").val() : $("input[name=btype]:checked").val();

        // BUILD DATA OBJECT
        user.bus = {
            name : bname,
            info : binfo,
            type : btype
        };

        transitionForm2(user);
    }

    // FORMAT AND STORE
    function submitForm(user) {
        $('#signupWrap').addClass('reloading');
        vs.api.user.newUser(user, function(data){
            var $login = $('<form style="display: none !important;" method="POST" action="/login"></form>').append('<input type="hidden" name="email" value="' + user.local.email + '"/>').append('<input type="hidden" name="password" value="' + user.local.password + '"/>');
            $login.submit();
        });
    }

    // CALL THE FIRST VALIDATE ON LOAD
    validateSignup1();

    $("form#signupForm1").unbind('submit');
    $("form#signupForm1").submit(function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        if ($('.has-success').length == 8) {
            $('#signupButton').attr('disabled', 'disabled');

            // PREVENT FORM FROM DEFAULT SUBMISSION VIA POST
            collectForm1();
            return false;
        }
    });
});