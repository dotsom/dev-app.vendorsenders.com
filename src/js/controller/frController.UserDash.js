$(document).ready(function() {
    var availableDayMarkup, $calendarCell, currentHID;
    var initArgs = {
      avail : serverData.avails,
      booking : serverData.bookings,
      waitlist : serverData.waitlists,
      situ : 'pageload-user'
    };

    calendarMdl.processCalendarView(initArgs);

    //notificationTray('#notification-wrap', serverData.uid);

    $('.changeMonth').click(function(){
        var dir = $(this).attr('data-direction');
        var changeMonthArgs = {
          month : serverData.currentMonth,
          year : serverData.currentYear,
          dir : dir,
          situ : 'pageload-user',
          plus : false
        };
        calendarMdl.changeMonthArrow(changeMonthArgs);
    });
});