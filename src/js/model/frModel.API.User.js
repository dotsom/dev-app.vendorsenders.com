var UserAPI = {};

$(document).ready(function() {

  UserAPI.getAll = function(cb) {
    $.ajax({
      type : 'GET',
      url : '/api/users',
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  }

  UserAPI.getById = function(uid, cb){

    var reqUrl = '/api/users/' + uid;

    $.ajax({
      type : 'GET',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  UserAPI.deleteById = function(uid, cb){

    var reqUrl = '/api/users/' + uid;

    $.ajax({
      type : 'DELETE',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  UserAPI.newUser = function(userData, cb){

    $.ajax({
      type : 'POST',
      url : '/api/users',
      data : userData,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  UserAPI.updateUser = function(userData, cb){

    $.ajax({
      type : 'PUT',
      url : '/api/users',
      data : userData,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

});