var WaitlistAPI = {};

$(document).ready(function() {

  WaitlistAPI.getAll = function(cb) {
    $.ajax({
      type : 'GET',
      url : '/api/waitlists',
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  }

  WaitlistAPI.getById = function(_id, cb){

    var reqUrl = '/api/waitlists/' + _id;

    $.ajax({
      type : 'GET',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  WaitlistAPI.getByAid = function(aid, cb){

    var reqUrl = '/api/waitlists/aid/' + aid;

    $.ajax({
      type : 'GET',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };


  WaitlistAPI.deleteById = function(_id, cb){

    var reqUrl = '/api/waitlists/' + _id;

    $.ajax({
      type : 'DELETE',
      url : reqUrl,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  WaitlistAPI.newWaitlist = function(waitlistData, cb){

    $.ajax({
      type : 'POST',
      url : '/api/waitlists',
      data : waitlistData,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

  WaitlistAPI.updateWaitlist = function(waitlistData, cb){

    $.ajax({
      type : 'PUT',
      url : '/api/waitlists',
      data : waitlistData,
      dataType : 'json',
      encode : true
    })

    .done(function(data) {
      if (cb) {
        cb(data);
      }
    });
  };

});