var calendarMdl = {},
    geocoder = new google.maps.Geocoder();
$(document).ready(function() {

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  direction
     * @param  situ
     * @param  plus
     */
    calendarMdl.changeMonthArrow = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        args.plus = typeof args.plus !== 'undefined' ? args.plus : true;

        calendarReq = {
            currentMonth : args.month,
            currentYear : args.year,
            direction : args.dir
        };

        calendarReq.reqType = args.plus ? 'changeMonthArrow' : 'changeMonthArrowNoPlus';

        if (typeof args.hostProfile != 'undefined') {
            if (args.hostProfile == true) {
                calendarReq.reqType = 'changeMonthHostView';
                calendarReq.hid = serverData.host._id;
            }
        }

        $.ajax({
            type : 'POST',
            url : '/calendar',
            data :  calendarReq,
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            var currentCalendarView = $('.calendarWrap').attr('data-calendar-view');

            if (args.situ == "pageload-avail") {
                var newArgs = {
                    avail: data.avails,
                    hid: typeof args.hid !== 'undefined' ? args.hid : ''
                };
                currentCalendarView = "4";
            } else {
                var newArgs = {
                    avail: data.avails,
                    booking: serverData.bookings,
                    hid: typeof args.hid !== 'undefined' ? args.hid : '',
                };
            }


            serverData.calendarString = data.newCalendar;
            serverData.currentMonth = data.newMonth;
            serverData.currentYear = data.newYear;
            serverData.avails = data.avails;

            switch (currentCalendarView) {
                case "1":
                    newArgs.situ = 'pageload-user';
                break;

                case "2":
                    newArgs.situ = 'pageload-user';
                break;

                case "3":
                    newArgs.situ = 'load-host';
                    newArgs.hid = $('#current-hid').attr('data-hid');
                break;

                case "4":
                    newArgs.situ = 'pageload-avail';
                    break;

                case "5":
                    newArgs.situ = 'pageload-host';
                    newArgs.bookings = serverData.bookings;
                    newArgs.avail = serverData.avails;
                break;

                case "6":
                    newArgs.situ = 'pageload-user-admin';
                break;

                case "7":
                    newArgs.situ = 'load-host-admin';
                    newArgs.hid = serverData.hid;
                break;
            }


            $('.calendarWrap').html(data.newCalendar);
            $('#monthName').html(serverData.Calendar.monthNames[serverData.currentMonth] + " " + serverData.currentYear);
            calendarMdl.processCalendarView(newArgs);
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  situ
     * @param  plus
     */
    calendarMdl.changeMonth = function(args) {
        args = typeof args !== 'undefined' ? args : {};
        args.plus = typeof args.plus !== 'undefined' ? args.plus : true;
        $.ajax({
            type : 'POST',
            url : '/calendar',
            data :  {
                reqType : args.plus ? 'changeMonth' : 'changeMonthNoPlus',
                currentMonth : serverData.currentMonth,
                currentYear : serverData.currentYear,
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            var newArgs = {};
            serverData.calendarString = data.newCalendar;
            serverData.currentMonth = data.newMonth;
            serverData.currentYear = data.newYear;
            serverData.avails = data.avails;
            newArgs.avail = data.avails;
            newArgs.situ = args.situ;
            newArgs.hid = typeof args.hid !== 'undefined' ? args.hid : '';

            $('.calendarWrap').html(data.newCalendar);
            $('#monthName').html(serverData.Calendar.monthNames[serverData.currentMonth] + " " + serverData.currentYear);
            calendarMdl.processCalendarView(newArgs);
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     */
    calendarMdl.findAvailability = function(args) {
        cargs = typeof args !== 'undefined' ? args : {};

        var availsToday = [];
        for (var i = 0; i < serverData.avails.length; i++) {
            if (serverData.avails[i].year === args.year && serverData.avails[i].month === args.month && serverData.avails[i].day === args.day) {
                availsToday.push(serverData.avails[i]);
            }
        }
        for (var i = 0; i < availsToday.length; i++) {
            for (var j = 0; j < serverData.hosts.length; j++) {
                if (availsToday[i].hid === serverData.hosts[j]._id) {
                    availsToday[i].hostInfo = serverData.hosts[j];
                }
            }
        }
        return availsToday;
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindFindAvailability = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        if (typeof args.el != 'undefined') {
            args.el.click(function(){
                var day = parseInt($(this).parent().parent().attr('data-day'));
                var findAvailArgs = {
                    day : day,
                    month: serverData.currentMonth,
                    year : serverData.currentYear
                };
                newArgs.avail = calendarMdl.findAvailability(findAvailArgs);
                newArgs.situ = 'display-avail';
                newArgs.day = day;
                calendarMdl.processSidebarView(newArgs);
            });
        } else {
            $('.view-avail').unbind();
            $('.view-avail').click(function(){
                var day = parseInt($(this).parent().parent().data('day'));
                var findAvailArgs = {
                    day : day,
                    month: serverData.currentMonth,
                    year : serverData.currentYear
                };
                newArgs.avail = calendarMdl.findAvailability(findAvailArgs);
                newArgs.situ = 'display-avail';
                newArgs.day = day;
                calendarMdl.processSidebarView(newArgs);
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.addAvailability = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        $.ajax({
            type        : 'POST',
            url         : '/calendar',
            data        :  {
                reqType : 'addAvail',
                month : args.month,
                year : args.year,
                day : args.day,
                start : args.start,
                end : args.end,
                hid : args.hid
            },
            dataType    : 'json',
            encode      : true
        })

        // RECEIVE AND DELEGATE TO FUNCTION
        .done(function(data) {
            if (data.status == 'success') {
                serverData.avails.push(data.avail);
                var newArgs = {
                    avail : data.avail,
                    situ : 'availAdded'
                };
                calendarMdl.processCalendarView(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddAvailability = function(args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        if (args.el) {
            $(args.el).click(function(){
                newArgs.year = '' + serverData.currentYear;
                newArgs.month = serverData.currentMonth < 10 ? '0' + serverData.currentMonth : '' + serverData.currentMonth;
                var day = $(this).parent().parent().data('day');
                newArgs.day = day < 10 ? '0' + day : '' + day;
                newArgs.hid = $('#selectedHost').val();
                newArgs.start = parseInt($('#startTime').val());
                newArgs.end = parseInt($('#endTime').val());

                if (newArgs.hid && newArgs.start && newArgs.end) {
                    if (newArgs.start < newArgs.end){
                        calendarMdl.addAvailability(newArgs);
                    } else {
                        var markup = calendarMdl.createModalMarkup('impossible-availability');
                        $('body').prepend(markup);
                        calendarMdl.bindExitX();
                    }
                }
            });
        } else {
            $('.add-booking').click(function(){
                newArgs.year = '' + serverData.currentYear;
                newArgs.month = serverData.currentMonth < 10 ? '0' + serverData.currentMonth : '' + serverData.currentMonth;
                var day = $(this).parent().parent().data('day');
                newArgs.day = day < 10 ? '0' + day : '' + day;
                newArgs.hid = $('#selectedHost').val();
                newArgs.start = parseInt($('#startTime').val());
                newArgs.end = parseInt($('#endTime').val());

                if (newArgs.hid && newArgs.start && newArgs.end) {
                    if (newArgs.start < newArgs.end){
                        calendarMdl.addAvailability(newArgs);
                    } else {
                        var markup = calendarMdl.createModalMarkup('impossible-availability');
                        $('body').prepend(markup);
                        calendarMdl.bindExitX();
                    }
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindMoreInfo = function(waitlist){
        $('.more-info').unbind('click');
        $('.more-info').click(function(e){

            var infotype = $(this).attr('infotype');
            var bid = $(this).parent().parent().attr('data-bid') + '';

            switch(infotype) {
                case 'host':
                    vs.api.booking.getById(bid, function(booking){
                        vs.api.host.getById(booking.hid, function(host){
                            vs.api.user.getById(booking.uid, function(user){
                                vs.api.availability.getById(booking.aid, function(avail){
                                    vs.api.file.getByUid(host._id, function(files){
                                        var markup = calendarMdl.createModalMarkupUserBooking(host, files, avail);
                                        $('body').prepend(markup);
                                        calendarMdl.bindExitX();
                                    });
                                });
                            });
                        });
                    });
                    break;

                case 'user':
                    vs.api.booking.getById(bid, function(booking){
                        vs.api.user.getById(booking.uid, function(user){
                            vs.api.availability.getById(booking.aid, function(avail){
                                var markup = calendarMdl.createModalMarkupHostBooking(user, avail);
                                $('body').prepend(markup);
                                calendarMdl.bindExitX();
                            });
                        });
                    });
                    break;

                case 'waitlisted':
                        var aid = $(this).parent().parent().attr('data-aid') + '';
                        var hid = $(this).parent().parent().attr('data-hid') + '';


                        vs.api.host.getById(hid, function(host){
                            vs.api.availability.getById(aid, function(avail){
                                vs.api.file.getByUid(host._id, function(files){
                                    var markup = calendarMdl.createModalMarkupUserBooking(host, files, avail);
                                    $('body').prepend(markup);
                                    calendarMdl.bindExitX();
                                });
                            });
                        });
                    break;
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  aid
     * @param  el
     * @param  day
     */
    calendarMdl.deleteAvailability = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        var markup = calendarMdl.createModalMarkup('delete-availability');
        $('body').prepend(markup);
        calendarMdl.bindExitX();
        $('.confirm-delete').click(function(e){

            $.ajax({
                type : 'POST',
                url : '/calendar',
                data : {
                    reqType : 'deleteAvail',
                    aid : args.aid
                },
                dataType    : 'json',
                encode      : true
            })

            // RECEIVE AND DELEGATE TO FUNCTION
            .done(function(data) {
                if (data.status == 'success') {
                    var oldMarkup = '<div class="day-number">' + args.day + '</div>';
                    oldMarkup += '<div class="day-icon-bar i2">';
                    oldMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';
                    var theAid = args.el.attr('aid');
                    for (var i = 0; i < serverData.avails.length; i++) {
                        if (serverData.avails[i]._id === theAid) {
                            serverData.avails.splice(i, 1);
                        }
                    }
                    args.el.removeClass('available')
                        .attr('aid', '')
                        .attr('start', '')
                        .attr('end', '')
                        .html(oldMarkup);
                    var newArgs = {el : args.el.find('.add-booking')};
                    calendarMdl.bindAddAvailability(newArgs);
                    $('.modal-overlay').remove();
                }
            });
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteAvailability = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        if (args.el) {
            $(args.el).click(function(){
                newArgs.el = $(this).parent().parent();
                newArgs.day = newArgs.el.find('.day-number').html();
                newArgs.aid = newArgs.el.attr('aid');

                if (newArgs.aid) {
                    calendarMdl.deleteAvailability(newArgs);
                }
            });
        } else {
            $('.remove-booking').click(function(){
                newArgs.el = $(this).parent().parent();
                newArgs.day = newArgs.el.find('.day-number').html();
                newArgs.aid = newArgs.el.attr('aid');

                if (newArgs.aid) {
                    calendarMdl.deleteAvailability(newArgs);
                }
            });
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.addBooking = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        var markup = calendarMdl.createModalMarkup('add-booking');
        $('body').prepend(markup);
        calendarMdl.bindExitX();
        $('.confirm-booking').click(function(e){

            $.ajax({
                type        : 'POST',
                url         : '/calendar',
                data        :  {
                    reqType : 'addBooking',
                    hid : args.hid,
                    aid : args.aid,
                    uid : serverData.uid,
                    type : serverData.thisUser.bus.type
                },
                dataType    : 'json',
                encode      : true
            })

            // RECEIVE AND DELEGATE TO FUNCTION
            .done(function(data) {
                if (data.status == 'success') {
                    serverData.bookings.push(data.booking);
                    var newArgs = {
                        avail : data.avail,
                        booking : data.booking,
                        situ : 'bookingAdded'
                    };
                    $('.host-info.host-visits').html('<strong>Recent Visits:</strong>' + (args.visitInfo.lastYear + 1) + ' in ' + serverData.currentYear);
                    calendarMdl.refreshBookingsServerData
                    calendarMdl.processCalendarView(newArgs);
                    $('.modal-overlay').remove();
                }
            });
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddBooking = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};
        $('.add-booking').unbind('click');
        $('.add-booking').click(function(){
            newArgs.hid = $(this).parent().parent().attr('data-hid');
            newArgs.aid = $(this).parent().parent().attr('data-aid');
            if (newArgs.hid && newArgs.aid) {

                var yearBegin = moment(serverData.currentYear + ' 01 01', 'YYYY MM DD').toDate();
                var yearEnd = moment((serverData.currentYear + 1) + ' 01 01', 'YYYY MM DD').toDate();

                vs.api.availability.checkMax(yearBegin, yearEnd, newArgs.hid, serverData.uid, function(visitInfo){
                    if (visitInfo.lastYear < visitInfo.maxVisits) {
                        newArgs.visitInfo = visitInfo;
                        calendarMdl.addBooking(newArgs);
                    } else {
                        var markup = calendarMdl.createModalMarkup('max-bookings');
                        $('body').prepend(markup);
                        calendarMdl.bindExitX();
                        $('.confirm-understanding').click(function(e){
                            $('.modal-overlay').remove();
                        });
                    }
                });
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  month
     * @param  year
     * @param  day
     * @param  hid
     * @param  start
     * @param  end
     */
    calendarMdl.deleteBooking = function(args) {
        args = typeof args !== 'undefined' ? args : {};

        var markup = calendarMdl.createModalMarkup('delete-booking');
        $('body').prepend(markup);
        calendarMdl.bindExitX();
        $('.confirm-delete').click(function(e){

            $.ajax({
                type        : 'POST',
                url         : '/calendar',
                data        :  {
                    reqType : 'deleteBooking',
                    bid : args.bid
                },
                dataType    : 'json',
                encode      : true
            })

            // RECEIVE AND DELEGATE TO FUNCTION
            .done(function(data) {
                if (data.status == 'success') {
                    var newArgs = {
                        situ : $('.calendarWrap').attr('data-calendar-view') == '6' || $('.calendarWrap').attr('data-calendar-view') == '1' ? 'bookingDeletedGeneral' : 'bookingDeletedFocus',
                        bid : args.bid,
                        aid : data.aid
                    };
                    $('.host-info.host-visits').html('<strong>Recent Visits:</strong>' + (args.visitInfo.lastYear - 1) + ' in ' + serverData.currentYear);
                    calendarMdl.refreshBookingsServerData();
                    calendarMdl.processCalendarView(newArgs);
                    $('.modal-overlay').remove();
                }
            });
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteBooking = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        $('.remove-booking').unbind('click');
        $('.remove-booking').click(function(){
            newArgs.bid = $(this).parent().parent().attr('data-bid');
            newArgs.hid = $(this).parent().parent().attr('data-hid');
            if (newArgs.bid && newArgs.hid) {
                var yearBegin = moment(serverData.currentYear + ' 01 01', 'YYYY MM DD').toDate();
                var yearEnd = moment((serverData.currentYear + 1) + ' 01 01', 'YYYY MM DD').toDate();

                vs.api.availability.checkMax(yearBegin, yearEnd, newArgs.hid, serverData.uid, function(visitInfo){
                    newArgs.visitInfo = visitInfo;
                    calendarMdl.deleteBooking(newArgs);
                });
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  aid
     * @param  uid
     * @param  hid
     */
    calendarMdl.addWaitlist = function (args){
        vs.api.waitlist.getByAid(args.aid, function(waitlist){
            if (waitlist.length == 0){
                vs.api.waitlist.create({
                    aid: args.aid,
                    uid: args.uid,
                    hid: args.hid
                }, function(waitlist){
                    var newArgs = {
                        situ : 'waitlistAdded',
                        aid : args.aid
                    };
                    calendarMdl.processCalendarView(newArgs);
                });
            } else {
                vs.api.waitlist.addUser({
                    aid: args.aid,
                    uid: args.uid
                }, function(waitlist){
                    var newArgs = {
                        situ : 'waitlistAdded',
                        aid : args.aid
                    };
                    calendarMdl.processCalendarView(newArgs);
                });
            }
        });
    }

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindAddWaitlist = function (args){
        args = typeof args !== 'undefined' ? args : {};
        var newArgs = {};

        $('.add-waitlist').unbind('click');
        $('.add-waitlist').click(function(){
            newArgs.aid = $(this).parent().parent().attr('data-aid');
            newArgs.hid = $(this).parent().parent().attr('data-hid');
            newArgs.uid = serverData.uid;
            if (newArgs.aid) {
                calendarMdl.addWaitlist(newArgs);
            }
        });
    };

    calendarMdl.deleteWaitlist = function (args){
        vs.api.waitlist.getByAid(args.aid, function(waitlist){
            if (waitlist.length != 0){
                vs.api.waitlist.removeUser({
                    aid: args.aid,
                    uid: args.uid
                }, function(waitlist){
                    if (waitlist.users.length < 1){
                        vs.api.waitlist.remove(args.aid, function(){
                            var newArgs = {
                                situ : 'waitlistDeleted',
                                aid : args.aid
                            };
                            calendarMdl.processCalendarView(newArgs);
                        });
                    } else {
                        var newArgs = {
                            situ : 'waitlistDeleted',
                            aid : args.aid
                        };
                        calendarMdl.processCalendarView(newArgs);
                    }
                });
            }
        });
    }

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.bindDeleteWaitlist = function (args){
        args = typeof args !== 'undefined' ? args : {};

        var newArgs = {};
        $('.remove-waitlist').unbind('.remove-waitlist');
        $('.remove-waitlist').click(function(){
            var $thisSquare = $(this).parent().parent();
            newArgs.aid = $thisSquare.attr('data-aid');
            newArgs.uid = serverData.uid;
            if (newArgs.aid) {
                calendarMdl.deleteWaitlist(newArgs);
            }
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  el
     */
    calendarMdl.refreshBookingsServerData = function (){
        vs.api.booking.getByUid(serverData.uid, function(bookings){
            serverData.bookings = bookings;
        });
    };

    // MARKUP LOOPS AND ALTERATIONS

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailSetAvail = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell,
                currentHID = $('#selectedHost').val();

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].hid === currentHID) {
                    newMarkup = '<span class="avail-info"><strong>';
                    newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                    newMarkup += ' - ';
                    newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                    newMarkup += '</strong></span>';
                    newMarkup += '<div class="day-number">';
                    newMarkup += args.avail[i].day;
                    newMarkup += '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';

                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    $calendarCell.addClass('available')
                      .attr('aid', args.avail[i]._id)
                      .attr('start', args.avail[i].start)
                      .attr('end', args.avail[i].end)
                      .html(newMarkup);
                }
            }
        })();
        if (cb) {
            cb();
        }
    };

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.addOneAvailSetAvail = function(args, cb) {
        (function(){
            var newMarkup,
                newArgs = {},
                $calendarCell;

            newMarkup = '<span class="avail-info"><strong>';
            newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
            newMarkup += ' - ';
            newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
            newMarkup += '</strong></span>';
            newMarkup += '<div class="day-number">';
            newMarkup += args.avail.day;
            newMarkup += '</div>';
            newMarkup += '<div class="day-icon-bar i2">';
            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';

            $calendarCell = $('td[data-day="' + args.avail.day + '"]');
            $calendarCell.addClass('available')
                .attr('aid', args.avail._id)
                .attr('start', args.avail.start)
                .attr('end', args.avail.end)
                .html(newMarkup);

            newArgs.el = 'td[data-day="' + args.avail.day + '"] .remove-booking';
        })();
        if (cb) {
            cb(args);
        }
    };

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailUserProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');

                if (!$calendarCell.hasClass('available')) {
                    newMarkup = '<div class="day-number">' + args.avail[i].day + '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';

                    if (moment().isBefore(moment('' + serverData.currentYear + '-' + (serverData.currentMonth + 1) + '-' + args.avail[i].day, 'YYYY-MM-DD'))) {
                        newMarkup += '<div class="icon i-sky view-avail"><i class="fa fa-search-plus"></i></div>';
                    }

                    newMarkup += '</div>'
                    $calendarCell.addClass('available')
                        .html(newMarkup);
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowAvailHostProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                newMarkup =  '<span class="avail-info"><strong>Open</strong>';
                newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                newMarkup += '</span>';
                newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                $calendarCell.addClass('available')
                    .attr('data-aid', args.avail[i]._id)
                    .html(newMarkup);
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    /**
     * The args object uses values for the following keys
     * @param  avail
     */
    calendarMdl.loopShowBookedHostProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.bookings.length; i++) {
                $calendarCell = $('td[data-aid="' + args.bookings[i].aid + '"]');
                newMarkup =  '<span class="avail-info"><strong>You\'re Booked!</strong>';
                for (var j = 0; j < args.avail.length; j++) {
                    if (args.avail[j]._id == args.bookings[i].aid){
                        newMarkup += reference.hours[args.avail[j].start][0] + ' ' + reference.hours[args.avail[j].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[j].end][0] + ' ' + reference.hours[args.avail[j].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-number">' + args.avail[j].day + '</div>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div infotype="user" class="icon i-dgray more-info"><i class="fa fa-info"></i></div></div>';
                    }
                }
                $calendarCell.addClass('available')
                    .attr('data-bid', args.bookings[i]._id)
                    .html(newMarkup);
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    calendarMdl.loopShowBookedUserProfile = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                for (var j = 0; j < args.booking.length; j++) {
                    if (args.avail[i]._id === args.booking[j].aid) {
                        for (var k = 0; k < serverData.hosts.length; k++) {
                            if (args.booking[j].hid == serverData.hosts[k]._id) {
                                var thisHost = serverData.hosts[k];
                            }
                        }
                        newMarkup = '<span class="booking-info"><strong>You\'re Booked!</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div infotype="host" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';

                        var twoWeeksAgo = moment('' + serverData.currentYear + '-' + (serverData.currentMonth + 1) + '-' + args.avail[i].day, 'YYYY-MM-DD').subtract(14, 'days').format('YYYY-MM-DD');

                        if (moment().isBefore(moment(twoWeeksAgo, 'YYYY-MM-DD'))) {
                            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i>';
                        }

                        newMarkup += '</div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';
                        $calendarCell.addClass('booked-you')
                            .attr('data-bid', args.avail[i].bid)
                            .attr('data-hid', args.avail[i].hid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    calendarMdl.loopShowBookedUserAdmin = function(args, cb) {
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                for (var j = 0; j < args.booking.length; j++) {
                    if (args.avail[i]._id === args.booking[j].aid) {
                        for (var k = 0; k < serverData.hosts.length; k++) {
                            if (args.booking[j].hid == serverData.hosts[k]._id) {
                                var thisHost = serverData.hosts[k];
                            }
                        }
                        newMarkup = '<span class="booking-info"><strong>You\'re Booked!</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div infotype="host" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                        newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i>';
                        newMarkup += '</div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';
                        $calendarCell.addClass('booked-you')
                            .attr('data-bid', args.avail[i].bid)
                            .attr('data-hid', args.avail[i].hid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    }

    calendarMdl.loopShowAvailUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                if (!$calendarCell.hasClass('available') && args.hid === args.avail[i].hid) {

                    var dateString = '' + serverData.currentYear + '-' + (serverData.currentMonth + 1) + '-' + args.avail[i].day;

                    newMarkup = '<span class="avail-info">';

                    if (moment().isBefore(moment(dateString, 'YYYY-MM-DD'))) {
                        newMarkup +=  '<strong>Available:</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                    }

                    newMarkup += '</span>';
                    newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';
                    newMarkup += '<div class="day-icon-bar i2">';

                    if (moment().isBefore(moment(dateString, 'YYYY-MM-DD'))) {
                        newMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div>';
                    }

                    newMarkup += '</div>';

                    $calendarCell.addClass('available')
                        .attr('data-aid', args.avail[i]._id)
                        .attr('data-hid', args.hid)
                        .html(newMarkup);
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.loopShowBookedUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    if ($calendarCell.hasClass('available') && args.hid === args.avail[i].hid) {

                        newMarkup =  '<span class="avail-info"><strong>';
                        newMarkup += isNaN(args.avail[i].vendorType) ? args.avail[i].vendorType :  reference.btypes[args.avail[i].vendorType];
                        newMarkup +=  ' Vendor</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<br/>Join Waitlist?</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div class="icon i-gray add-waitlist"><i class="fa fa-clock-o"></i></div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                        $calendarCell.removeClass('available').addClass('booked')
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            args.booking = serverData.bookings;
            cb(args);
        }
    };

    calendarMdl.loopShowWaitlistedUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            args.waitlist = serverData.waitlists;

            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    for (var j in args.waitlist) {
                        if (args.avail[i]._id == args.waitlist[j].aid && $('#current-hid').attr('data-hid') == args.waitlist[j].hid) {
                            $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                            newMarkup =  '<span class="avail-info"><strong>On Waitlist</strong>';
                            newMarkup += '</span>';
                            newMarkup += '<div class="day-icon-bar i2">';
                            newMarkup += '<div infotype="waitlisted" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                            newMarkup += '<div class="icon i-coral remove-waitlist"><i class="fa fa-minus"></i></div></div>';
                            newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                            $calendarCell.removeClass('booked').addClass('waitlisted')
                                .html(newMarkup);
                        }
                    }
                }
            }
        })();

        if (cb) {
            args.booking = serverData.bookings;
            cb(args);
        }
    };

    calendarMdl.loopShowBookedSelfUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;


            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    if (args.hid === args.avail[i].hid && serverData.uid === args.avail[i].uid) {

                        newMarkup = '<span class="booking-info"><strong>You\'re Booked!</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div infotype="host" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';

                        var twoWeeksAgo = moment('' + serverData.currentYear + '-' + (serverData.currentMonth + 1) + '-' + args.avail[i].day, 'YYYY-MM-DD').subtract(14, 'days').format('YYYY-MM-DD');

                        if (moment().isBefore(moment(twoWeeksAgo, 'YYYY-MM-DD'))) {
                            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i>';
                        }

                        newMarkup += '</div></div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                        $calendarCell.removeClass('available').addClass('booked')
                            .attr('data-bid', args.avail[i].bid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.loopShowBookedSelfUserHostAdmin = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;


            for (var i = 0; i < args.avail.length; i++) {
                if (args.avail[i].booked) {
                    $calendarCell = $('td[data-day="' + args.avail[i].day + '"]');
                    if (args.hid === args.avail[i].hid && serverData.uid === args.avail[i].uid) {

                        newMarkup = '<span class="booking-info"><strong>You\'re Booked!</strong>';
                        newMarkup += reference.hours[args.avail[i].start][0] + ' ' + reference.hours[args.avail[i].start][1];
                        newMarkup += ' - ';
                        newMarkup += reference.hours[args.avail[i].end][0] + ' ' + reference.hours[args.avail[i].end][1];
                        newMarkup += '</span>';
                        newMarkup += '<div class="day-icon-bar i2">';
                        newMarkup += '<div infotype="host" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                        newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div>';
                        newMarkup += '</div>';
                        newMarkup += '<div class="day-number">' + args.avail[i].day + '</div>';

                        $calendarCell.removeClass('available').addClass('booked admin')
                            .attr('data-bid', args.avail[i].bid)
                            .html(newMarkup);
                    }
                }
            }
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.addOneBookingUserHost = function(args, cb){
        (function(){
            var newMarkup,
                $calendarCell;

            $calendarCell = $('td[data-day="' + args.avail.day + '"]');
            newMarkup =  '<span class="avail-info"><strong>You\'re Booked!</strong>';
            newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
            newMarkup += ' - ';
            newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
            newMarkup += '</span>';
            newMarkup += '<div class="day-icon-bar i2">';
            newMarkup += '<div infotype="host" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
            newMarkup += '<div class="icon i-coral remove-booking"><i class="fa fa-minus"></i></div></div>';
            newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

            $calendarCell.removeClass('available').addClass('booked')
                .attr('data-bid', args.avail.bid)
                .html(newMarkup);

        })();

        if (cb) {
            cb(args);
        }

    };

    calendarMdl.deleteOneBookingUserHost = function(args, cb){
        (function(args){
        var newMarkup,
            $calendarCell,
            thisHid = $('#current-hid').attr('data-hid');


            for (var i = 0; i < serverData.avails.length; i++) {
                if (serverData.avails[i]._id == args.aid) {
                    args.avail = serverData.avails[i];
                }
            }

            $calendarCell = $('td[data-bid="' + args.bid + '"]');

            if (args.avail.hid == thisHid) {
                newMarkup =  '<span class="avail-info"><strong>Available:</strong>';
                newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
                newMarkup += '</span>';
                newMarkup += '<div class="day-icon-bar i2">';
                newMarkup += '<div class="icon i-sky add-booking"><i class="fa fa-plus"></i></div></div>';
                newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

                $calendarCell.addClass('available')
                    .removeClass('booked')
                    .removeClass('booked-you')
                    .attr('data-aid', args.aid)
                    .attr('data-hid', thisHid)
                    .attr('data-bid', '')
                    .html(newMarkup);

                args.el = $('td[data-aid="' + args.aid + '"] .add-booking');

            } else {
                newMarkup = '<div class="day-number">' + args.avail.day + '</div>';

                $calendarCell.removeClass('booked')
                    .removeClass('booked-you')
                    .attr('data-aid', '')
                    .attr('data-hid', '')
                    .attr('data-bid', '')
                    .html(newMarkup);
            }
        })(args);

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.deleteOneBookingUser = function(args, cb){
        (function(args){
        var newMarkup,
            $calendarCell,
            thisHid = $('#current-hid').attr('data-hid');


            for (var i = 0; i < serverData.avails.length; i++) {
                if (serverData.avails[i]._id == args.aid) {
                    args.avail = serverData.avails[i];
                }
            }

            $calendarCell = $('td[data-bid="' + args.bid + '"]');
            newMarkup = '<div class="day-number">' + args.avail.day + '</div>';
            newMarkup += '<div class="day-icon-bar i2">';
            newMarkup += '<div class="icon i-sky view-avail"><i class="fa fa-search-plus"></i></div>';
            newMarkup += '</div>'
            $calendarCell.addClass('available')
                .removeClass('booked-you')
                .html(newMarkup);

             args.el = $calendarCell.find('.view-avail');
        })(args);

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.addOneWaitlistUserHost = function(args, cb){
        vs.api.availability.getById(args.aid, function(avail){
            (function(args){
                var newMarkup,
                    $calendarCell;

                    args.avail = avail;
                    $calendarCell = $('td[data-day="' + avail.day + '"]');
                    newMarkup =  '<span class="avail-info"><strong>On Waitlist</strong>';
                    newMarkup += '</span>';
                    newMarkup += '<div class="day-icon-bar i2">';
                    newMarkup += '<div infotype="waitlisted" class="icon i-dgray more-info"><i class="fa fa-info"></i></div>';
                    newMarkup += '<div class="icon i-coral remove-waitlist"><i class="fa fa-minus"></i></div></div>';
                    newMarkup += '<div class="day-number">' + avail.day + '</div>';

                    $calendarCell.removeClass('booked').addClass('waitlisted')
                        .html(newMarkup);

                    args.el = $('td[data-day="' + avail.day + '"]').find('.remove-waitlist');
            })(args);

            if (cb) {
                cb(args);
            }
        });
    };

    calendarMdl.deleteOneWaitlistUserHost = function(args, cb){

        (function(args){
        var newMarkup,
            $calendarCell,
            thisHid = $('#current-hid').attr('data-hid');

            for (var i = 0; i < serverData.avails.length; i++) {
                if (serverData.avails[i]._id === args.aid) {
                    args.avail = serverData.avails[i];
                }
            }

            $calendarCell = $('td[data-aid="' + args.aid + '"]');

                newMarkup =  '<span class="avail-info"><strong>';
                newMarkup += isNaN(args.avail.vendorType) ? args.avail.vendorType :  reference.btypes[args.avail.vendorType];
                newMarkup +=  ' Vendor</strong>';
                newMarkup += reference.hours[args.avail.start][0] + ' ' + reference.hours[args.avail.start][1];
                newMarkup += ' - ';
                newMarkup += reference.hours[args.avail.end][0] + ' ' + reference.hours[args.avail.end][1];
                newMarkup += '</span>';
                newMarkup += '<br/>Join Waitlist?</span>';
                newMarkup += '<div class="day-icon-bar i2">';
                newMarkup += '<div class="icon i-gray add-waitlist"><i class="fa fa-clock-o"></i></div></div>';
                newMarkup += '<div class="day-number">' + args.avail.day + '</div>';

                $calendarCell.addClass('booked')
                    .removeClass('waitlisted')
                    .attr('data-aid', args.aid)
                    .attr('data-hid', thisHid)
                    .html(newMarkup);

                args.el = $('td[data-aid="' + args.aid + '"] .add-waitlist');
        })(args);

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.displaySideHostsWithAvail = function(args, cb){
        (function(){
            var newMarkup = '';

            for (var i = 0; i < args.avail.length; i++) {
                    newMarkup += '<li data-hid="' + args.avail[i].hid + '">';
                    newMarkup += '<h3 class="view-host-avail">' + args.avail[i].hostInfo.company.name + '</h3>';
                    newMarkup += '<span class="host-info">$' + args.avail[i].hostInfo.company.fee + ' | ' + args.avail[i].hostInfo.company.city + ', ' + args.avail[i].hostInfo.company.state + '</span>';
                    newMarkup += '</li>';
            }

            $('#resultsWrap').html(newMarkup);
        })();

        if (cb) {
            cb(args);
        }
    };

    calendarMdl.displaySideHostInfo = function(args, cb){
        (function(){
            var newMarkup;

            var yearBegin = moment(serverData.currentYear + ' 01 01', 'YYYY MM DD').toDate();
            var yearEnd = moment((serverData.currentYear + 1) + ' 01 01', 'YYYY MM DD').toDate();

            for (var i = 0; i < serverData.hosts.length; i++) {
                if (serverData.hosts[i]._id === args.hid) {
                    var that = i;
                    vs.api.availability.checkMax(yearBegin, yearEnd, args.hid, serverData.uid, function(visitInfo){

                        //NAME
                        newMarkup = '<div class="sidebar-header"><span id="sidebarStepBack" class="exit-x fa fa-arrow-left"></span><h2 class="sidebar-title">' + serverData.hosts[that].company.name + '</h2></div><div class="inner-sidebar">';
                        //ADDRESS
                        newMarkup += '<span class="host-info host-address"><strong>Address:</strong>' + serverData.hosts[that].company.address + '<br/>' + serverData.hosts[that].company.city + ', ' + serverData.hosts[that].company.state + ' ' + serverData.hosts[that].company.zip + '</span>';
                        //SIZE
                        newMarkup += '<span class="host-info host-size"><strong>Size:</strong>' + reference.companySize[serverData.hosts[that].company.size] + ' employees</span>';
                        //MAX
                        newMarkup += '<span class="host-info host-max"><strong>Allowed Visits:</strong>' + serverData.hosts[that].restrictions.max + ' per Year</span>';
                        //VISITS
                        newMarkup += '<span class="host-info host-visits"><strong>Recent Visits:</strong>' + visitInfo.lastYear + ' in ' + serverData.currentYear + '</span>';
                        //FEE
                        newMarkup += '<span class="host-info host-fee"><strong>Fee:</strong>$' + serverData.hosts[that].company.fee + '</span>';
                        //RESTRICTIONS
                        newMarkup += '<span class="host-info host-fee"><strong>Other Restrictions:</strong>' + serverData.hosts[that].restrictions.items + '</span></div>';
                        newMarkup += '<div style="display:none" id="current-hid" data-hid="' + serverData.hosts[that]._id + '"></div>';

                        var prevState = $('.calendarWrap').attr('data-calendar-view') == "7" ? "6" : "2";
                        var prevSidebar = $('.sidebar').children().clone(true);
                        var prevCalendar = $('.calendarWrap').children().clone(true);
                        var prevHeader = $('.calendar-header').children().clone(true);
                        var prevMonth = serverData.currentMonth;
                        var prevYear = serverData.currentYear;

                        $('.sidebar').html(newMarkup);

                        $('#sidebarStepBack').click(function(){
                            $('.calendarWrap').html(prevCalendar);
                            $('.calendarWrap').attr('data-calendar-view', prevState);
                            $('.sidebar').html(prevSidebar);
                            $('.calendar-header').html(prevHeader);
                            serverData.currentMonth = prevMonth;
                            serverData.currentYear = prevYear;
                            $('#iconWrap').resetSearchFilterIcons({
                                name: function(){
                                  searchSidebar.bindNameSearch(serverData.hosts)
                                },
                                fee: function(){
                                  searchSidebar.handleFeeSearch(serverData.hosts)
                                },
                                zip: function(){
                                  searchSidebar.bindZipSearch(serverData.hosts)
                                },
                                size: function(){
                                  searchSidebar.bindSizeSearch(serverData.hosts)
                                }
                            });
                        });

                        if (cb) {
                            cb(args.args1);
                        }
                    });
                }
            }
        })();
    };


    calendarMdl.bindSideHostAvail = function(args, cb){
        (function(args){
            var args1 = {},
                args2 = {};
            args1.month = serverData.currentMonth;
            args1.year = serverData.currentYear;
            args1.situ = 'load-host';
            args1.plus = false;
            args1.avail = serverData.avails;

            $('.view-host-avail').unbind('click');
            $('.view-host-avail').click(function(){
                args1.hid = $(this).parent().attr('data-hid');
                serverData.hid = $(this).parent().attr('data-hid');
                args2.avail = args.avail;
                switch ($('.calendarWrap').attr('data-calendar-view')) {

                    case '6':
                        args2.situ = 'display-host-admin';
                        args1.situ = 'load-host-admin';
                        break;

                    case '7':
                        args2.situ = 'load-host-admin';
                        args1.situ = 'load-host-admin';
                        break;

                    default:
                        args2.situ = 'display-host'
                }

                args2.hid = args1.hid;

                if (cb) {
                    cb(args1, args2);
                }
            });
        })(args);
    };

    calendarMdl.createModalMarkupUserBooking = function(host, files, avail){
        var markup = '<div class="modal-overlay">\
            <div class="well col-sm-4 modal-box">\
              <span id="exit-modal" class="exit-x fa fa-times"></span>\
              <h4>Booking Information</h4>\
              <p>Business Name: ' + host.company.name + '</br>\
              Date: ' + reference.months[avail.month] + ' ' + avail.day + ', ' + avail.year + '</br>\
              Time: ' + reference.hours[avail.start][0] + ' ' + reference.hours[avail.start][1] + ' - \
              ' + reference.hours[avail.end][0] + ' ' + reference.hours[avail.end][1] + '</br>\
              Location: <a target="_blank" href="http://maps.google.com/?q=' + host.company.address +
              ', ' + host.company.city + ' ' + host.company.state +
              ', ' + host.company.zip + '">' + host.company.address +
              ', ' + host.company.city + ' ' + host.company.state +
              ', ' + host.company.zip + '</a></p>\
              <h4>Images, Files, and Instructions</h4>\
              <ul class="file-list">';

        for(var i = 0; i < files.length; i++){
            markup += '<li class="file-list-item"><a target="_blank" href="' + files[i].path + '">' + files[i].name +'</a></li>';
        }

        markup += '</ul>\
            </div>\
          </div>';

        return markup;
    };

    calendarMdl.createModalMarkupHostBooking = function(user, avail){
        var markup = '<div class="modal-overlay">\
            <div class="well col-sm-4 modal-box">\
              <span id="exit-modal" class="exit-x fa fa-times"></span>\
              <h4>Booking Information</h4>\
              <p>Vendor Name: ' + user.bus.name + '</br>\
              Vendor Info: ' + user.bus.info + '</br>\
              Vendor Sells: ' + reference.btypes[parseInt(user.bus.type)] + '</br>\
              Date: ' + reference.months[avail.month] + ' ' + avail.day + ', ' + avail.year + '</br>\
              Time: ' + reference.hours[avail.start][0] + ' ' + reference.hours[avail.start][1] + ' - \
              ' + reference.hours[avail.end][0] + ' ' + reference.hours[avail.end][1] + '</p>';

        return markup;
    };

    calendarMdl.createModalMarkup = function(situation){
        var markup = '';

        switch (situation) {

            case 'impossible-availability':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Unable to Create Availability</h4>\
                              <p>Availabilities cannot end before they begin!</p></div></div>';
                break;

            case 'delete-availability':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Are you sure?</h4>\
                              <p>Are you sure that you want to delete this availability?  All associated waitlists and bookings will be deleted as well.</p>\
                              <button class="confirm-delete bttn bttn-primary">Delete</button></div></div>';
                break;

            case 'delete-booking':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Are you sure?</h4>\
                              <p>Are you sure that you want to delete this booking?  Once a booking is deleted, other vendors will have the opportunity to claim this availability.</p>\
                              <button class="confirm-delete bttn bttn-primary">Delete</button></div></div>';
                break;

            case 'delete-user':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Are you sure?</h4>\
                              <p>Are you sure that you want to delete this user?  This is not reversable.</p>\
                              <button class="confirm-delete bttn bttn-primary">Delete</button></div></div>';
                break;

            case 'delete-host':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Are you sure?</h4>\
                              <p>Are you sure that you want to delete this host?  This is not reversable.</p>\
                              <button class="confirm-delete bttn bttn-primary">Delete</button></div></div>';
                break;

            case 'add-booking':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Book it?</h4>\
                              <p>Are you sure that you want to book this date?</p>\
                              <button class="confirm-booking bttn bttn-primary">Continue</button></div></div>';
                break;

            case 'max-bookings':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Oh No!</h4>\
                              <p>It seems like you\'ve already met your maximum bookings for the year at this property.  Please unschedule a different booking at this property in order to book this date.</p>\
                              <button class="confirm-understanding bttn bttn-primary">Close</button></div></div>';
                break;

            case 'contact':
                markup += '<div class="modal-overlay">\
                            <div class="well col-sm-4 modal-box">\
                              <span id="exit-modal" class="exit-x fa fa-times"></span>\
                              <h4>Contact Us</h4>\
                              <p>You can email us at <a href="mailto:info@vendorsenders.com">info@vendorsenders.com</a>.  You can also reach Marcy at 215-588-9140 and Lisa at 267-446-8929.  Whichever way you choose, we\'ll be happy to answer any question you may have.</p></div></div>';
                break;


        }
        return markup;
    };

    calendarMdl.bindExitX = function(){
        $('#exit-modal').click(function(){
          $('.modal-overlay').remove();
        });
    };

    /**
     * The args object can take values for the following keys
     * @param  avail
     * @param  hid
     * @param  situ
     */
    calendarMdl.processCalendarView = function(args){

        args = typeof args !== 'undefined' ? args : {};
        args.hid = typeof args.hid !== 'undefined' ? args.hid : '';
        var newArgs = {},
            availableDayMarkup;


        switch (args.situ) {
            case 'pageload-avail':
                calendarMdl.loopShowAvailSetAvail(args, function(){
                    calendarMdl.bindAddAvailability();
                    calendarMdl.bindDeleteAvailability();
                });
            break;

            case 'availAdded':
                calendarMdl.addOneAvailSetAvail(args, function(newArgs){
                    calendarMdl.bindDeleteAvailability(newArgs);
                });
            break;

            case 'pageload-user':
                calendarMdl.loopShowAvailUserProfile(args, function(args){
                    calendarMdl.loopShowBookedUserProfile(args, function(args){
                        calendarMdl.bindFindAvailability(args);
                        calendarMdl.bindDeleteBooking(args);
                        calendarMdl.bindMoreInfo();
                    });
                });
            break;

            case 'pageload-user-admin':
                calendarMdl.loopShowAvailUserProfile(args, function(args){
                    calendarMdl.loopShowBookedUserAdmin(args, function(args){
                        calendarMdl.bindFindAvailability(args);
                        calendarMdl.bindDeleteBooking(args);
                        calendarMdl.bindMoreInfo();
                    });
                });
            break;

            case 'pageload-host':
                calendarMdl.loopShowAvailHostProfile(args, function(args){
                    calendarMdl.loopShowBookedHostProfile(args);
                    calendarMdl.bindMoreInfo();
                });
            break;

            case 'load-host':
                calendarMdl.loopShowAvailUserHost(args, function(args){
                    calendarMdl.loopShowBookedUserHost(args, function(args){
                        calendarMdl.loopShowBookedUserProfile(args, function(args){
                            calendarMdl.loopShowBookedSelfUserHostAdmin(args, function(args){
                                calendarMdl.loopShowWaitlistedUserHost(args, function(args){
                                    calendarMdl.bindMoreInfo();
                                    calendarMdl.bindMoreInfo(true);
                                    calendarMdl.bindAddBooking(args);
                                    calendarMdl.bindDeleteBooking(args);
                                    calendarMdl.bindAddWaitlist(args);
                                    calendarMdl.bindDeleteWaitlist(args);
                                });
                            });
                        });
                    });
                });
            break;

            case 'load-host-admin':
                calendarMdl.loopShowAvailUserHost(args, function(args){
                    calendarMdl.loopShowBookedUserHost(args, function(args){
                        calendarMdl.loopShowBookedUserAdmin(args, function(args){
                                calendarMdl.loopShowWaitlistedUserHost(args, function(args){
                                    calendarMdl.bindMoreInfo();
                                    calendarMdl.bindMoreInfo(true);
                                    calendarMdl.bindAddBooking(args);
                                    calendarMdl.bindDeleteBooking(args);
                                    calendarMdl.bindAddWaitlist(args);
                                    calendarMdl.bindDeleteWaitlist(args);
                                });
                        });
                    });
                });
            break;

            case 'bookingAdded':
                calendarMdl.addOneBookingUserHost(args, function(args){
                    calendarMdl.bindDeleteBooking(args);
                    calendarMdl.bindMoreInfo();
                });
            break;

            case 'bookingDeletedGeneral':
                calendarMdl.deleteOneBookingUser(args, function(args){
                    calendarMdl.bindFindAvailability(args);
                });
            break;

            case 'bookingDeletedFocus':
                calendarMdl.deleteOneBookingUserHost(args, function(args){
                    calendarMdl.bindAddBooking(args);
                });
            break;

            case 'waitlistAdded':
                calendarMdl.addOneWaitlistUserHost(args, function(args){
                    calendarMdl.bindDeleteWaitlist(args);
                    calendarMdl.bindMoreInfo(true);
                });
            break;

            case 'waitlistDeleted':
                calendarMdl.deleteOneWaitlistUserHost(args, function(args){
                    calendarMdl.bindAddWaitlist(args);
                });
            break;
        }
    };

    /**
     * The args object can take values for the following keys
     * @param  avail
     * @param  hid
     * @param  situ
     */
    calendarMdl.processSidebarView = function(args){
        args = typeof args !== 'undefined' ? args : {};
        args.hid = typeof args.hid !== 'undefined' ? args.hid : '';
        var newArgs = {};

        switch (args.situ) {
            case 'display-avail':
                $('.calendarWrap').attr('data-calendar-view', '2');
                calendarMdl.displaySideHostsWithAvail(args, function(args){
                    calendarMdl.bindSideHostAvail(args, function(args1, args2){
                        args2.args1 = args1;
                        calendarMdl.processSidebarView(args2);
                    });
                });
            break;

            case 'display-avail-admin':
                $('.calendarWrap').attr('data-calendar-view', '2');
                calendarMdl.displaySideHostsWithAvail(args, function(args){
                    calendarMdl.bindSideHostAvail(args, function(args1, args2){
                        args1.situ = 'load-host-admin';
                        args2.args1 = args1;
                        calendarMdl.processSidebarView(args2);
                    });
                });
            break;

            case 'display-host':
                $('.calendarWrap').attr('data-calendar-view', '3');
                calendarMdl.displaySideHostInfo(args, function(args){
                    calendarMdl.changeMonth(args);
                });
            break;

            case 'display-host-admin':
                $('.calendarWrap').attr('data-calendar-view', '7');
                calendarMdl.displaySideHostInfo(args, function(args){
                    calendarMdl.changeMonth(args);
                });
            break;
        }
    };
});