reference = {};

// COMPANY SIZE
reference.companySize = [];
reference.companySize[1] = "1-10";
reference.companySize[2] = "11-50";
reference.companySize[3] = "51-100";
reference.companySize[4] = "101-200";
reference.companySize[5] = "201-500";
reference.companySize[6] = "501-1000";
reference.companySize[7] = "1001-10,000";
reference.companySize[8] = "10,001+";

reference.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

reference.hours = [
  ['12', 'AM'],
  ['1', 'AM'],
  ['2', 'AM'],
  ['3', 'AM'],
  ['4', 'AM'],
  ['5', 'AM'],
  ['6', 'AM'],
  ['7', 'AM'],
  ['8', 'AM'],
  ['9', 'AM'],
  ['10', 'AM'],
  ['11', 'AM'],
  ['12', 'PM'],
  ['1', 'PM'],
  ['2', 'PM'],
  ['3', 'PM'],
  ['4', 'PM'],
  ['5', 'PM'],
  ['6', 'PM'],
  ['7', 'PM'],
  ['8', 'PM'],
  ['9', 'PM'],
  ['10', 'PM'],
  ['11', 'PM']
]

reference.btypes = [
  'skip this one',
  'Home Made Food',
  'Packaged Food',
  'Honey, Candy, and Nuts',
  'Jewelry',
  'Candles',
  'Clothing',
  'Cosmetics',
  'Soaps',
  'Nursery',
  'Flowers',
  'Home Goods',
  'Photography',
  'Gardening',
  'Hand Crafted',
  'Marketing',
  'Fashion Accessories',
  'Fashion Accessories',
  'Licenced Items'
];

reference.filterMarkup = {
  name: '<div id="name-search" class="form-group">'
+         '<input class="form-control" type="text" placeholder="Start typing a company name"/>'
+       '</div>',

  size: '<div id="size-search" class="form-group">'
  +       '<select class="form-control">'
  +         '<option selected="selected" value="">Company Size</option>'
  +         '<option value="1">1-10</option>'
  +         '<option value="2">11-50</option>'
  +         '<option value="3">51-100</option>'
  +         '<option value="4">101-200</option>'
  +         '<option value="5">201-500</option>'
  +         '<option value="6">501-1000</option>'
  +         '<option value="7">1001-10,000</option>'
  +         '<option value="8">10,001+</option>'
  +       '</select>'
  +     '</div>',

  fee:  '',

  zip:  '<div id="zip-search" class="form-group">'
  +         '<input class="form-control" type="text" placeholder="type your zip code"/>'
  +         '<span class="bttn bttn-primary location">'
  +           '<i class="fa fa-search"></i> Search'
  +         '</span>'
  +       '</div>',
};

reference.htmlEncode = function(val){
  return $('<div/>').text(val).html();
};

reference.htmlDecode = function(val){
  return $('<div/>').html(val).text();
};

reference.distance = function(lat1,lon1,lat2,lon2){
  var R = 3959; // miles
  var dLat = (lat2-lat1) * Math.PI / 180;
  var dLon = (lon2-lon1) * Math.PI / 180;
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d;
};

reference.compareDistance = function(a, b){
  if (a.company.distance < b.company.distance) {
    return -1;
  }
  if (a.company.distance > b.company.distance) {
    return 1;
  }
  return 0;
};

if (typeof vs != 'undefined') {
  vs.ref = reference;
}